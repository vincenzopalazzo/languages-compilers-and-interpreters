// Author Vincenzo Palazzo v.palazzo@stundeti.unipi.it
#include <stdio.h>

int max(int *x, int *y)
{
	if (x == NULL || y == NULL) {
		return -1;
	}
	printf("%d\n", 1);
	if (*x < *y)
		return *y;
	return *x;
}

int main()
{
	int *x;
	x = NULL;
	int *y;
	y = NULL;
	int a = 1;
	y = &a;
	a = max(x, y);
	printf("%d\n", a);

	x = &a;
	a = max(x, y);
	printf("%d\n", a);
	return 0;
}