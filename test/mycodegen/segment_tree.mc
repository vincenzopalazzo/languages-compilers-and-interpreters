/**
 * Segment tree data structure implementation with MicroC language
 * developer to test an academics compiler of Master class
 * The compiler is available on Docker Hub
 * https://hub.docker.com/repository/docker/vincenzopalazzo/microclang
 *
 * Copyright (C) 2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

int left_index(int index) { return index * 2; }

int right_index(int index) { return (index * 2) + 1; }

void build(int tree[], int input[], int start, int left, int right)
{
	if (left == right) {
		tree[start] = left;
		return;
	}
	int middle_point = (left + right) / 2;
	int left_point = left_index(start);
	int right_point = right_index(start);
	build(tree, input, left_point, left, middle_point);
	build(tree, input, right_point, (middle_point + 1), right);
	int segment_left = tree[left_point];
	int segment_right = tree[right_point];
	if (input[segment_left] <= input[segment_right])
		tree[start] = segment_left;
	else
		tree[start] = segment_right;
}

int range_minimum_query(int tree[], int input[], int start, int left, int right,
			int query_left, int query_right)
{
	if ((query_left > right) || (query_right < left))
		return -1;
	if ((left >= query_left) && (right <= query_right))
		return tree[start];
	int middle_point = (left + right) / 2;
	int left_point = left_index(start);
	int right_point = right_index(start);

	int segment_left =
	    range_minimum_query(tree, input, left_point, left, middle_point,
				query_left, query_right);
	int segment_right =
	    range_minimum_query(tree, input, right_point, (middle_point + 1),
				right, query_left, query_right);

	if (segment_left == -1)
		return segment_right;
	if (segment_right == -1)
		return segment_left;

	if (input[segment_left] <= input[segment_right])
		return segment_left;

	return segment_right;
}

void range_minimum_problem(int inputs[], int start[], int end[], int size_input,
			   int size_query, int result[])
{

	// 4 * N is the size of segment tree
	int segment_tree[28];
	build(segment_tree, inputs, 1, 0, (size_input - 1));

	for (int i = 0; i < size_query; i++) {
		int index =
		    range_minimum_query(segment_tree, inputs, 1, 0,
					(size_input - 1), start[i], end[i]);
		result[i] = inputs[index];
	}
}

int main()
{
	// 18, 17, 13, 19, 15, 11, 20
	int input[7];
	input[0] = 18;
	input[1] = 17;
	input[2] = 13;
	input[3] = 19;
	input[4] = 15;
	input[5] = 11;
	input[6] = 20;
	int start[2];
	int end[2];
	start[0] = 4;
	end[0] = 6;
	start[1] = 1;
	end[1] = 3;
	int result[2];

	range_minimum_problem(input, start, end, 7, 2, result);

	for (int i = 0; i < 2; i++)
		print(result[i]);
	return 0;
}
