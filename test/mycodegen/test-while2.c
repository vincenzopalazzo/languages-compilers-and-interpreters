// Author Vincenzo Palazzo v.palazzo1@studenti.unipit.iter
#include <stdio.h>

void increment(int *a) { (*a)++; }

int main()
{
	int a;
	a = 0;
	while (a < 20) {
		increment(&a);
		printf("%d\n", a);
	}
	return 0;
}
