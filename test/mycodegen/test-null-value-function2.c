// Author Vincenzo Palazzo v.palazzo@stundeti.unipi.it
#include <stdbool.h>
#include <stdio.h>

int max(bool *x, bool *y)
{
	if (x == NULL || y == NULL) {
		return -1;
	}
	if (*x == *y)
		return 1;
	return 0;
}

int main()
{
	bool *x = NULL;
	bool *y = NULL;
	bool a = true;
	y = &a;
	printf("%d\n", max(x, y));
	x = &a;
	printf("%d\n", max(x, y));
	return 0;
}