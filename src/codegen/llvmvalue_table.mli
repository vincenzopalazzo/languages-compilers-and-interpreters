(* *
   * MicroC Compiler developer with University of Pisa for
   * courses Languages, Compilers and Interpreters class.
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
   * and all professors of the Languages, Compilers and Interpreters class
   * https://github.com/lillo/compiler-course-unipi
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *)

exception DuplicateEntry
exception NotFoundInScope

type dec

val empty_table : dec

val begin_block : dec -> string -> dec

val end_block : dec -> dec

val add_entry : Ast.identifier -> Llvm.llvalue -> dec -> bool -> unit

val lookup : Ast.identifier -> dec -> Llvm.llvalue

val get_name_scope : dec -> string

val set_end_block: dec -> bool -> unit

val is_ended: dec -> bool

val is_function: dec -> bool