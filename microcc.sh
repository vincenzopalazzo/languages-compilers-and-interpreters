# MicroC Compiler developer with University of Pisa for
# courses Languages, Compilers and Interpreters class.
# Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
# and all professors of the Languages, Compilers and Interpreters class
# https://github.com/lillo/compiler-course-unipi
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/bin/bash

# prints colored text
print_style () {

    if [ "$2" == "info" ] ; then
        COLOR="96m";
    elif [ "$2" == "success" ] ; then
        COLOR="92m";
    elif [ "$2" == "warning" ] ; then
        COLOR="93m";
    elif [ "$2" == "danger" ] ; then
        COLOR="91m";
    else #default color
        COLOR="0m";
    fi

    STARTCOLOR="\e[$COLOR";
    ENDCOLOR="\e[0m";

    printf "$STARTCOLOR%b$ENDCOLOR" "$1";
}

linkMicroCProgram() {
    clang-10 a.s rt-support.o -lm
    if [ "$?" -eq 0 ]; then
        rm -f *.o *.s *.bc
    else
        echo "Error during link library with microc"
    fi
}

compilerSDK() {
    clang-10 -c -Wall sdk/rt-support.c
    if [ "$?" -eq 0 ]; then
        linkMicroCProgram
    else
        echo "Error during compiling SDK library"
    fi
}

runLLVMCompiler() {
    llc-10 -relocation-model=pic *.bc
    if [ "$?" -eq 0 ]; then
        compilerSDK
    else
        echo "Error during compiling LLVM byte code"
    fi
}

## Compiler MicroC command
./microcc -c -O $1
if [ "$?" -eq 0 ]; then
    runLLVMCompiler
fi
