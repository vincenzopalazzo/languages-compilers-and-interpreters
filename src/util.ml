(**
  * MicroC Compiler developer with University of Pisa for
  * courses Languages, Compilers and Interpreters class.
  *
  * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
  * and all professors of the Languages, Compilers and Interpreters class
  * https://github.com/lillo/compiler-course-unipi
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**)

exception Syntax_error of string

exception Lexing_error of string

exception Semantic_error of string

let get_lexing_position lexbuf =
  let p = Lexing.lexeme_start_p lexbuf in
  let line_number = p.Lexing.pos_lnum in
  let column = p.Lexing.pos_cnum - p.Lexing.pos_bol + 1 in
  (line_number, column)

let raise_syntax_error lexbuf msg =
  let pos = get_lexing_position lexbuf in
  let m = Printf.sprintf "%d:%d: %s" (fst pos) (snd pos) msg in
  raise (Syntax_error m)

let raise_lexer_error lexbuf msg =
  let pos = get_lexing_position lexbuf in
  let m = Printf.sprintf "%d:%d: %s" (fst pos) (snd pos) msg in
  raise (Lexing_error m)

let raise_semantic_error (startp, endp) msg =
  let start_line_number = startp.Lexing.pos_lnum in
  let start_column_number =
    startp.Lexing.pos_cnum - startp.Lexing.pos_bol + 1
  in
  let end_line_number = endp.Lexing.pos_lnum in
  let end_column_number = endp.Lexing.pos_cnum - endp.Lexing.pos_bol + 1 in
  let line =
    if start_line_number = end_line_number then
      string_of_int start_line_number
    else Printf.sprintf "%d-%d" start_line_number end_line_number
  in
  let column =
    if start_column_number = end_column_number then
      string_of_int start_column_number
    else Printf.sprintf "%d-%d" start_column_number end_column_number
  in
  let log = Printf.sprintf "%s:%s: %s" line column msg in
  raise (Semantic_error log)

let rec convert_type_to_str ast_typ =
  match ast_typ with
  | Ast.TypInt -> "int"
  | Ast.TypBool -> "bool"
  | Ast.TypChar -> "char"
  | Ast.TypArray (tipe, _) ->
      Printf.sprintf "%s[]" (convert_type_to_str tipe)
  | Ast.TypPoint tipe -> Printf.sprintf "*%s" (convert_type_to_str tipe)
  | Ast.TypVoid -> "void"
  | Ast.TypNull -> "NULL"

let compare_type tipe_one tipe_two =
  match tipe_one with
  | Ast.TypArray (array_tipe1, _) -> (
    match tipe_two with
    | Ast.TypArray (array_tipe2, _) -> array_tipe1 = array_tipe2
    | _ -> false )
  | Ast.TypPoint point_tipe1 -> (
    match tipe_two with
    | Ast.TypPoint point_tipe2 -> 
      point_tipe1 = point_tipe2 || point_tipe1 = TypNull 
      || point_tipe2 = TypNull
    | _ -> false )
  | _ -> tipe_one = tipe_two

let operation_between_pointers tipe_one tipe_two =
  match tipe_one with
  | Ast.TypPoint point_tipe1 -> (
    match tipe_two with Ast.TypPoint point_tipe2 -> true | _ -> false )
  | _ -> false
