// author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int a;
	a = getint();

	if (a > 0) {
		print(a);
	} else {
		print(-1);
	}
	return 0;
}
