FROM ubuntu:xenial
LABEL mantainer="Vincenzo Palazzo vincenzopalazzodev@gmail.com"

WORKDIR /home/micro

RUN apt-get update && apt-get install -y \
   software-properties-common \
   xz-utils \
   build-essential \
   wget \
   curl \
   cmake \
   m4 \
   pkg-config \
   python2.7 \
   libmysqlclient-dev \
   protobuf-compiler \
   apt-transport-https \
   ca-certificates \
   ocaml-findlib \
   apt-utils

# Install Python
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update && apt-get install -y \
        python3.7 \
        python3-pip
RUN python3.7 -m pip install pip
RUN apt-get update && apt-get install -y \
        python3.7-distutils \
        python3-setuptools
RUN python3.7 -m pip install pip --upgrade pip

# Insatall opam
RUN add-apt-repository ppa:avsm/ppa
RUN apt-get update && apt-get install -y opam

# install Llvm
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key| apt-key add -
RUN add-apt-repository "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-10 main"
RUN apt-get update && apt-get install -y clang-10 \ 
      lldb-10 \
      lld-10 \ 
      libllvm-10-ocaml-dev \ 
      libllvm10 \ 
      llvm-10 \
      llvm-10-dev \
      llvm-10-doc \
      llvm-10-examples \llvm-10-runtime \ 
      clang-10 \
      clang-tools-10 \
      clang-10-doc \
      libclang-common-10-dev \
      libclang-10-dev \
      libclang1-10 \
      clang-format-10 \
      clangd-10 \
      libfuzzer-10-dev \
      lldb-10 \
      lld-10 \
      libc++-10-dev \ 
      libc++abi-10-dev \
      libomp-10-dev

# Build MicroC
WORKDIR /home/micro/microclang/build
COPY . /home/micro/microclang/build

# Init Opam
RUN opam init --auto-setup --compiler=4.11.1 --disable-sandboxing -y
RUN opam install -y ocamlfind menhir ppx_deriving llvm easy_logging
RUN pip3 install -r requirements.txt

# Source next line https://www.reddit.com/r/ocaml/comments/4zzqkq/help_with_getting_a_docker_container_set_up_with/
RUN opam config exec -- make
RUN make install
RUN opam config exec -- make clean
# RUN rm -rf src # only when you push the image in docker hub
# RUN rm test_unit.py   # only when you push the image in docker hub
