// author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int at(int a[], int index)
{
	int elem_at;
	elem_at = a[index];
	return elem_at;
}

int main()
{
	int a[10];
	print(at(a, 0));
	return 0;
}
