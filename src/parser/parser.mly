(* *
   * MicroC Compiler developer with University of Pisa for
   * courses Languages, Compilers and Interpreters class.
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
   * and all professors of the Languages, Compilers and Interpreters class
   * https://github.com/lillo/compiler-course-unipi
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *)
%{
    open Ast
    open Printf
    open Easy_logging
    open Util
    open Lexing

    let (|@|) node loc = { node = node; loc = loc }
    let logger = Logging.make_logger "Parser" Info [Cli Debug]
%}

// 1. LOWEST
// 2. MEDIUM
// 3. HIGHT
%token <int> INT
%token <char> CHAR
%token <string> ID
%token <bool> BOOL
%token IF ELSE RETURN WHILE FOR DO
%token INT_KEY CHAR_KEY BOOL_KEY VOID NULL
%token LEFT_ROUND "(" RIGHT_ROUND ")"
%token LEFT_BRACKET "[" RIGHT_BRACKET "]"
%token LEFT_BRACE "{" RIGHT_BRACE "}"
%token AMPEREAND "&" COMMA SEMICOLON
%token PLUS MINUS SLASH STAR MOD EQ "=" LT GT
%token LE GE EQEQ NE NOT AND OR
%token EOF PREIN "++" PREDEC "--"
%token ABSUM "+=" ABSUB "-=" ABMOL "*=" ABDIV "/=" ABMOD "%="

%right EQ
%left OR
%left AND
%left EQEQ NE
%nonassoc GE GT LE LT
%left PLUS MINUS
%left STAR SLASH MOD
%nonassoc NOT
%nonassoc LEFT_BRACKET

%start program

%type <Ast.program> program

%%

program:
  | d = list(decl) EOF {logger#debug "List of declaration"; Prog d}
;

decl:
  | t = globvar SEMICOLON
          {logger#debug "Gobal variable declaration\n"; t |@| $loc }
  | t = type_def i = ID LEFT_ROUND params = parameters RIGHT_ROUND b = block
          {logger#debug "Function declaration\n"; (Fundecl({typ=t; fname=i; formals=params; body=b})) |@| $loc }
;

parameters:
       { logger#debug "Empty function parameter\n"; [] }
  | p = parameter
       {logger#debug "Sigle function parameter\n"; [p] }
  | p = parameter COMMA ps = parameters
       {logger#debug "List parameters\n"; p::ps }
;

parameter:
  | t = type_def i = ID
          {logger#debug "Parameter found\n"; (t, i)}
  | t = type_def i = ID value = delimited("[", option(INT), "]")
          {logger#debug "Parameter found\n"; (TypArray(t, value), i)}
;

type_def:
  | INT_KEY
      {logger#debug "int (var type) found"; TypInt}
  | BOOL_KEY
      {logger#debug "bool (var type) found"; TypBool}
  | CHAR_KEY
      {logger#debug "char (var type) found"; TypChar}
  | VOID
      {logger#debug "void type found"; TypVoid}
  | t = type_def STAR
      {logger#debug "Type pointer found"; TypPoint(t)}
;

globvar:
  | ty = type_def id = ID
      {logger#debug "Global variable normal type"; Vardec(ty, id) }
  | ty = type_def id = ID value = delimited("[", option(INT), "]")
      {logger#debug "Global variable array"; Vardec(TypArray(ty, value), id) }
  | dec = globvar "=" e = exp
      {logger#debug "Global Assignment"; DecAssign(dec |@| $loc, e)}
;

block:
  | sts = delimited("{", statements, "}") { logger#debug "Body function found\n"; Block(sts) |@| $loc }
  | st = statement { logger#debug "Body function found without { }\n"; Block(st::[]) |@| $loc }
;

statements:
  | stms = list(statement) { logger#debug "List of statements\n"; stms }
;

statement:
  | t = type_def i = ID SEMICOLON
    {logger#debug "Declaration stm found"; Dec(t, i) |@| $loc}
  | t = type_def i = ID value = delimited("[", option(INT), "]") SEMICOLON
    {logger#debug "Declaration stm found"; Dec(TypArray(t, value), i) |@| $loc}
  | stm = stm { Stmt(stm) |@| $loc }
;

stm:
  | e = exp SEMICOLON
    {logger#debug "Expression found\n"; Expr(e) |@| $loc }
  | IF e = delimited("(", exp, ")") ifs = block els = elseblk
    { logger#debug "if-else found\n"; If(e, ifs, els) |@| $loc }
  | WHILE e = delimited("(", exp, ")") blk = block
     {logger#debug "While found\n"; While(e, blk) |@| $loc }
  | DO blk = block WHILE exp = delimited("(", exp, ")") SEMICOLON
     { logger#trace "Do While found\n"; DoWhile(exp, blk) |@| $loc }
  | RETURN e = option(exp) SEMICOLON
    { logger#debug "Return statment found\n"; Return(e) |@| $loc }
  | FOR "(" dec = option(exp) SEMICOLON 
    cond = option(exp) SEMICOLON inc = option(exp) ")" "{"
    blk = list(statement) "}"
    { logger#debug "For stm with {} found";
      let unwrap_increment increment =
      logger#debug "Call to unwrap_increment";
       match increment with
       | Some v -> 
        logger#debug "Add element to list";
        blk@[(Stmt(Expr(v) |@| $loc) |@| $loc)]
    | None -> blk 
       in
      let unwrap_stm stmt =
      match stmt with
      | Some v -> [(Stmt(Expr(v) |@| $loc) |@| $loc)]
      | None -> []
      in
      let unwrap_cond cond =
      match cond with
      | Some v -> v
      | None -> (BLiteral(true) |@| $loc)
      in
      let with_cond = unwrap_cond cond in
      let stms = unwrap_stm dec in
      let new_stm = stms@[(Stmt(While(with_cond, Block(unwrap_increment inc) |@| $loc) |@| $loc) |@| $loc)] in
      logger#debug "Before add STMTs size %d" (List.length stms);
      Block(new_stm) |@| $loc}
  | FOR "(" dec = option(exp) SEMICOLON cond = option(exp) SEMICOLON inc = option(exp) ")" blk = statement
    { logger#debug "For stm without {} found";
       let unwrap_increment increment =
              match increment with
              | Some v -> [blk; (Stmt(Expr(v) |@| $loc) |@| $loc)]
              | None -> [blk]
       in
       let unwrap_stm stmt =
              match stmt with
              | Some v -> [(Stmt(Expr(v) |@| $loc) |@| $loc)]
              | None -> []
       in
       let unwrap_cond cond =
              match cond with
              | Some v -> v
              | None -> (BLiteral(true) |@| $loc)
       in
       let with_cond = unwrap_cond cond in
       let stms = unwrap_stm dec in
       let new_stmts = stms@[(Stmt(While(with_cond, Block(unwrap_increment inc) |@| $loc) |@| $loc) |@| $loc)] in
       Block(new_stmts) |@| $loc}
;

elseblk:
 | {logger#debug "if wihout else found"; Block([]) |@| $loc }
 | ELSE els = block { els }
;

exp:
| t = type_def i = ID EQ e = exp
    {logger#debug "Declaration stm found"; 
       DecAssig(
       (Dec(t, i) |@| $loc),
       (Assign((AccVar(i) |@| $loc), e) |@| $loc) 
       ) |@| $loc}
| re = rexpr
        {re}
| le = lexpr
        {logger#debug "access to value found"; Access(le) |@| $loc }
| id = lexpr EQ y = exp
        {logger#debug "Assignment found"; Assign(id, y) |@| $loc }
| x = exp GE y = exp
        {logger#debug ">= found"; BinaryOp(Geq, x, y) |@| $loc }
| x = exp LE y = exp
        {logger#debug "<= found"; BinaryOp(Leq, x, y) |@| $loc }
| x = exp GT y = exp
        {logger#debug "> found"; BinaryOp(Greater, x, y) |@| $loc }
| x = exp LT y = exp
        {logger#debug "< found"; BinaryOp(Less, x, y) |@| $loc }
| x = exp EQEQ y = exp
        {logger#debug "Check expression with == found"; BinaryOp(Equal, x, y) |@| $loc }
| x = exp PLUS y = exp
        {logger#debug "Expression x + y found"; BinaryOp(Add, x, y) |@| $loc }
| x = exp MINUS y = exp
        {logger#debug "Expression x - y found"; BinaryOp(Sub, x, y) |@| $loc }
| x = exp STAR y = exp
        {logger#debug "Expression x * y found"; BinaryOp(Mult, x, y) |@| $loc }
| x = exp SLASH y = exp
        {logger#debug "Expression x / y found"; BinaryOp(Div, x, y) |@| $loc }
| x = exp MOD y = exp
        {logger#debug "Expression x mod y found"; BinaryOp(Mod, x, y) |@| $loc}
| x = exp AND y = exp
       {logger#debug "Expression x && y found"; BinaryOp(And, x, y) |@| $loc }
| x = exp OR y = exp
        {logger#debug "Expression x || y found"; BinaryOp(Or, x, y) |@| $loc }
| x = exp NE y = exp
        {logger#debug "Expression x != y found"; BinaryOp(Neq, x, y) |@| $loc }
| NOT e = exp
        {logger#debug "Expression ! x found"; UnaryOp(Not, e) |@| $loc }
| MINUS e = exp
        {logger#debug "Negative expression -x found"; UnaryOp(Neg, e) |@| $loc}
| le = delimited("(", exp, ")")
        {logger#debug "Expression between ( )"; le }
| AMPEREAND e = lexpr
        {logger#debug "Dereference operator &"; Addr(e) |@| $loc}
| acc = lexpr ABSUM e = exp
        {logger#debug "Abbreviation assigment with sum";
           Assign(acc, BinaryOp(Add, (Access(acc) |@| $loc), e) |@| $loc) |@| $loc}
| acc = lexpr ABSUB e = exp
        {logger#debug "Abbreviation assignment with subtraction";
           Assign(acc, BinaryOp(Sub, (Access(acc) |@| $loc), e) |@| $loc) |@| $loc}
| acc = lexpr ABMOL e = exp
        {logger#debug "Abbreviation assigment with moltiplication";
           Assign(acc, BinaryOp(Mult, (Access(acc) |@| $loc), e) |@| $loc) |@| $loc}
| acc = lexpr ABDIV e = exp
        {logger#debug "Abbreviation assigment with division";
           Assign(acc, BinaryOp(Div, (Access(acc) |@| $loc), e) |@| $loc) |@| $loc}
| acc = lexpr ABMOD e = exp
        {logger#debug "Abbreviation assigment with mod operation";
           Assign(acc, BinaryOp(Mod, (Access(acc) |@| $loc), e) |@| $loc) |@| $loc}
| acc = lexpr "++"
        {logger#trace "Abbreviation increment with i++";
          UnaryOp(PostInc, Access(acc) |@| $loc) |@| $loc}
| "++" acc = lexpr
        {logger#trace "Abbreviation increment with i++";
          UnaryOp(PreInc, Access(acc) |@| $loc) |@| $loc}
| acc = lexpr "--" 
        {logger#trace "Abbreviation decrement with i--";
          UnaryOp(PostDec, Access(acc) |@| $loc) |@| $loc}
| "--" acc = lexpr
        {logger#trace "Abbreviation decrement with i--";
          UnaryOp(PreDec, Access(acc) |@| $loc) |@| $loc}
;

lexpr:
 | id = ID
          {logger#debug "Identifier found %s" id; AccVar(id) |@| $loc }
 | STAR e = lexpr
          {logger#debug "Pointer found *p"; AccDeref(Access(e) |@| $loc) |@| $loc }
 | le = lexpr e = delimited("[", exp, "]")
          {logger#debug "Array at position"; AccIndex(le, e) |@| $loc }
 | "(" e = lexpr ")"
          {logger#debug "lExpression between ( expr )"; e}
;

rexpr:
  | vl = aexpr
    {vl}
  | id = ID exps = delimited("(", separated_list(COMMA, exp), ")")
    {logger#debug "Function call found"; Call(id, exps) |@| $loc }
;

aexpr:
  | vl = INT
           {logger#debug "Int val found %d" vl; ILiteral(vl) |@| $loc }
  | vl = CHAR
           {logger#debug "Char val found %c" vl; CLiteral(vl) |@| $loc }
  | vl = BOOL
           {logger#debug "Boolean value found"; BLiteral(vl) |@| $loc }
  | NULL 
        {logger#debug "Null value found"; NullLiteral(TypPoint(TypNull)) |@| $loc }
;
