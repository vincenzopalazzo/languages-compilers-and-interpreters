// Author Vincenzo Palazzo v.palazzo@stundeti.unipi.it
#include <stdio.h>

int main()
{
	int *x = NULL;
	int *y = NULL;
	int a = 1;
	y = &a;
	if (x == NULL && y == NULL) {
		printf("%d\n", -1);
	} else {
		printf("%d\n", 0);
	}

	return 0;
}