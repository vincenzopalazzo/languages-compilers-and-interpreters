// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
#include <stdio.h>

int main()
{
	int a;
	a = 0;
	do {
		a++;
		printf("%d\n", a);
	} while (a < 20);
	return 0;
}
