// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int a;
	a = 0;
	print(a);

	a += 12;
	print(a);

	a -= 2;
	print(a);

	a *= 2;
	print(a);

	a /= 2;
	print(a);

	if ((a % 2) == 0) {
		print(0);
	} else {
		print(-1);
	}

	a %= 2;
	print(0);
	return 0;
}
