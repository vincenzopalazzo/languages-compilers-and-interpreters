(* *
   * MicroC Compiler developer with University of Pisa for 
   * courses Languages, Compilers and Interpreters class. 
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com 
   * and all professors of the Languages, Compilers and Interpreters class 
   * https://github.com/lillo/compiler-course-unipi 
   *
   * This program is free software; you can redistribute it and/or 
   * modify it under the terms of the GNU General Public License 
   * as published by the Free Software Foundation; either version 2 
   * of the License, or (at your option) any later version. 
   * 
   * This program is distributed in the hope that it will be useful, 
   * but WITHOUT ANY WARRANTY; without even the implied warranty of 
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
   * GNU General Public License for more details. 
   * 
   * You should have received a copy of the GNU General Public License 
   * along with this program; if not, write to the Free Software 
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
  *)

type binop =
  | Add
  | Sub
  | Mult
  | Div
  | Mod
  | Equal
  | Neq
  | Less
  | Leq
  | Greater
  | Geq
  | And
  | Or
  | Comma
[@@deriving show]

type uop = Neg | Not | PreInc | PostInc | PreDec | PostDec
[@@deriving show]

type identifier = string [@@deriving show]

type position = Lexing.position * Lexing.position

let dummy_pos = (Lexing.dummy_pos, Lexing.dummy_pos)

type 'a annotated_node = {loc: position [@opaque]; node: 'a}
[@@deriving show]

type typ =
  | TypInt (* Type int *)
  | TypBool (* Type bool *)
  | TypChar (* Type char *)
  | TypArray of typ * int option (* Array type *)
  | TypPoint of typ (* Pointer type *)
  | TypVoid (* Type void *)
  | TypNull
[@@deriving show]

and expr = expr_node annotated_node

and expr_node =
  | Access of access (* x or *p or a[e] *)
  | Assign of access * expr (* x=e or *p=e or a[e]=e *)
  | Addr of access (* &x or &*p or &a[e] *)
  | ILiteral of int (* Integer literal *)
  | CLiteral of char (* Char literal *)
  | BLiteral of bool (* Bool literal *)
  | NullLiteral of typ (* NUll literal *)
  | UnaryOp of uop * expr (* Unary primitive operator *)
  | BinaryOp of binop * expr * expr (* Binary primitive operator *)
  | Call of identifier * expr list (* Function call f(...) *)
  | DecAssig of stmtordec * expr (* int x = 12*)
[@@deriving show]

and access = access_node annotated_node

and access_node =
  | AccVar of identifier (* Variable access x *)
  | AccDeref of expr (* Pointer dereferencing *p *)
  | AccIndex of access * expr (* Array indexing a[e] *)
[@@deriving show]

and stmt = stmt_node annotated_node

and stmt_node =
  | If of expr * stmt * stmt (* Conditional *)
  | While of expr * stmt (* While loop *)
  | DoWhile of expr * stmt (* Do While loop*)
  | Expr of expr (* Expression statement e; *)
  | Return of expr option (* Return statement *)
  | Block of stmtordec list (* Block: grouping and scope *)
[@@deriving show]

and stmtordec = stmtordec_node annotated_node

and stmtordec_node =
  | Dec of typ * identifier (* Local variable declaration *)
  | Stmt of stmt (* A statement *)
[@@deriving show]

type fun_decl =
  {typ: typ; fname: string; formals: (typ * identifier) list; body: stmt}
[@@deriving show]

type topdecl = topdecl_node annotated_node

and topdecl_node =
  | Fundecl of fun_decl
  | Vardec of typ * identifier
  | DecAssign of topdecl * expr
[@@deriving show]

type program = Prog of topdecl list [@@deriving show]
