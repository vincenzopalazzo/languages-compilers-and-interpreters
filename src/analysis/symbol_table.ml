(* *
   * MicroC Compiler developer with University of Pisa for
   * courses Languages, Compilers and Interpreters class.
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
   * and all professors of the Languages, Compilers and Interpreters class
   * https://github.com/lillo/compiler-course-unipi
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *)
open Ast
open Easy_logging

exception DuplicateEntry

exception NotFoundInScope

let logger = Logging.make_logger "SymbolTable" Info [Cli Debug]

type dec =
  { tipe: Ast.typ
  ; variables: (Ast.identifier, Ast.typ) Hashtbl.t
  ; parent: dec option }

let empty_table =
  {tipe= Ast.TypVoid; variables= Hashtbl.create 0; parent= None}

let begin_block table tipe =
  logger#debug "Starting scope" ;
  {tipe; variables= Hashtbl.create 0; parent= Some table}

let end_block table =
  match table.parent with
  | Some v ->
      logger#debug "Block with %d variable deleted"
        (Hashtbl.length table.variables) ;
      v
  | None -> table

let rec lookup symbol table =
  let wanted_var = Hashtbl.find_opt table.variables symbol in
  match wanted_var with
  | Some v -> v
  | None -> (
    match table.parent with
    | Some v -> lookup symbol v
    | None -> raise NotFoundInScope )

let add_entry symbol info table =
  let variable = Hashtbl.find_opt table.variables symbol in
  match variable with
  | Some v -> raise DuplicateEntry
  | None -> Hashtbl.add table.variables symbol info

let get_type symbol = symbol.tipe
