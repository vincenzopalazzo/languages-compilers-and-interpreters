// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
#include <stdbool.h>
#include <stdio.h>
int A = 12;
bool B = false;
char C = 'c';

int main()
{
	printf("%d\n", A);
	if (B == false)
		printf("%d\n", 0);
	if (C == 'c')
		printf("%d\n", 0);
	return 0;
}
