# Micro Clang

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/vincenzopalazzo/languages-compilers-and-interpreters/master?style=for-the-badge) 
![Docker Pulls](https://img.shields.io/docker/pulls/vincenzopalazzo/microclang?style=for-the-badge)

> If you think like a computer, write C language make sense”
>                    - Linus Torvalds, Nothing better than C

## Overview

- [Introduction]()
- [Build with]()
- [Cody Style]()
- [License]()

### Introduction

MicroC is an academics compiler of a C subset language full stack-based.  It is a complete programming language that contains primitive types and complex types as arrays. It based on LLVM infrastructure, and it is full developed in the Ocaml language.

### Build with

The compiler is full implemented with Ocaml language version `4.11.1` with the following dependence

- ocamlbuild v0.14.0 
- ocamlformat v0.16.0
- llvm v10.0.0 :bangbang:
- menhir v20201216
- easy_logging v0.7.1
- ppx_deriving v5.2.1

p.s: All the :bangbang: are strictly required with the same version

The command to install all the dependence required after config the 
OCaml environment are the following

```bash
opam install -y ocamlfind menhir ppx_deriving llvm easy_logging
```

You can build the compiler with docker-compose with the following command in the root directory

```bash
docker-compose up
```

In addition, you can use the pull the Docker image with the following commands

```bash
docker pull vincenzopalazzo/microclang
docker run -t -i --rm vincenzopalazzo/microclang bash
>> microcc -help
------------ OUTPUT --------------------
Usage:	microcc [-p|-s|-d|-c] <source_file>

  -p Parse and print AST
  -s Perform semantic checks
  -d Print the generated code
  -o Place the output into file
  -O Optimize the generated code
  -c Compile (default)
  -help  Display this list of options
  --help  Display this list of options

```

Docker Hub: https://hub.docker.com/repository/docker/vincenzopalazzo/microclang

### Cody Style

#### Ocaml Code Style

The repository contains a file `.ocamlformat` and the `ocamlformat` profile is used, the line guide of the profile is
the following

>The ocamlformat profile aims to take advantage of the strengths of a parsetree-based auto-formatter and to limit the consequences of the weaknesses imposed by the current implementation. This is a style which optimizes for what the formatter can do best, rather than to match the style of any existing code. Instead of familiarity, the focus is on legibility, keeping the common cases reasonably compact while attempting to avoid confusing formatting in corner cases.

The command to run ocamlformat is `ocamlformat --inplace path/file/*.ml`.

#### MicroC Code Style

The repository contains a file `.clang-format` and the `LLVM` style is used, is chosen this type of code style we want to maintain all the code the same to
improve the readability of the program. For this reason, all the code was formatted with the following command on the root directory `clang-format -i path/file/*.mc`.

The same code style is the same as [C-lightning](https://github.com/ElementsProject/lightning) project.

### License

<div align="center">
  <img src="https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png" width="150" height="150"/>
</div>

MicroC Compiler developer with University of Pisa for
courses Languages, Compilers and Interpreters class.

Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
and all professors of the Languages, Compilers and Interpreters class
https://github.com/lillo/compiler-course-unipi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

