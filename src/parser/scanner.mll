(*
* MicroC Compiler developer with University of Pisa for
* courses Languages, Compilers and Interpreters class.
* Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
* and all professors of the Languages, Compilers and Interpreters class
* https://github.com/lillo/compiler-course-unipi
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*)
{
    open Lexing
    open Parser
    open Easy_logging

    exception SyntaxError of string

    let create_hashtable size init =
        let tbl = Hashtbl.create size in
        List.iter (fun (key, data) -> Hashtbl.add tbl key data) init;
        tbl

    let keywords =
        create_hashtable 8 [
            ("if", IF);
            ("else", ELSE);
            ("return", RETURN);
            ("for", FOR);
            ("do", DO);
            ("while", WHILE);
            ("int", INT_KEY);
            ("char", CHAR_KEY);
            ("void", VOID);
            ("bool", BOOL_KEY);
            ("NULL", NULL);
        ]

    let logger = Logging.make_logger "Scanner" Info [Cli Debug]
}

let digit = ['0' - '9']
let integer = digit+

let white_space = [' ' '\t']
let next_line =   '\r' | '\n' | "\r\n"
let identifier = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let ascii = (("'")(( [' ' - '~' ] ) as char_val)("'"))

rule next_token = parse
    | white_space+       {logger#debug "White space"; next_token lexbuf}
    | next_line+         {logger#debug "Next line"; Lexing.new_line lexbuf; next_token lexbuf}
    | "/*"               {logger#debug "Start read a possible comment"; comment lexbuf }
    | "//"               {logger#debug "Comment on single line"; singleline_comment lexbuf}
    | '+'                {logger#debug "+ -> PLUS"; PLUS}
    | '-'                {logger#debug "- -> MINUS"; MINUS}
    | '*'                {logger#debug "* -> STAR"; STAR}
    | '/'                {logger#debug "/ -> SLASH"; SLASH}
    | "%"                {logger#debug "MOD -> MOD"; MOD}
    | "<="               {logger#debug "<= -> LE"; LE}
    | ">="               {logger#debug ">= -> GE"; GE}
    | "=="               {logger#debug "== -> EQEQ"; EQEQ}
    | "!="               {logger#debug "!= -> NE"; NE}
    | '='                {logger#debug "= -> EQ"; EQ}
    | '<'                {logger#debug "< -> LT"; LT}
    | '>'                {logger#debug "> -> GT"; GT}
    | '!'                {logger#debug "! -> NOT"; NOT}
    | "&&"               {logger#debug "&& -> AND"; AND}
    | "||"               {logger#debug "|| -> OR"; OR}
    | '&'                {logger#debug "& -> AMPEREAND"; AMPEREAND}
    | '{'                {logger#debug "{ -> LEFT_BRACE"; LEFT_BRACE}
    | '}'                {logger#debug "} -> RIGHT_BRACE"; RIGHT_BRACE}
    | ';'                {logger#debug "; -> SEMICOLON"; SEMICOLON}
    | ','                {logger#debug ", -> COMMA"; COMMA}
    | '('                {logger#debug "( -> LEFT_ROUND"; LEFT_ROUND}
    | ')'                {logger#debug ") -> RIGHT_ROUND"; RIGHT_ROUND}
    | ']'                {logger#debug "] -> RIGHT_BRACKET"; RIGHT_BRACKET}
    | '['                {logger#debug "[ -> LEFT_BRACKET"; LEFT_BRACKET}
    | "++"               {logger#trace "++ -> PREIN"; PREIN}
    | "--"               {logger#trace "-- -> PREDEC"; PREDEC}
    | "+="               {logger#trace "+= -> ABSUM"; ABSUM}
    | "-="               {logger#trace "-= -> ABSUB"; ABSUB}
    | "*="               {logger#trace "*= -> ABMOL"; ABMOL}
    | "/="               {logger#trace "/= -> ABDIV"; ABDIV}
    | "%="               {logger#trace "MOD= -> ABMOD"; ABMOD}
    | "true" as vl       {logger#debug "Boolean value %s" vl; BOOL(bool_of_string vl)}
    | "false" as vl      {logger#debug "Boolean value %s" vl; BOOL(bool_of_string vl)}
    | integer as i       {logger#debug "Integer found %s" i; INT(int_of_string i)}
    | ascii              {logger#debug "Char found %c" char_val; CHAR(char_val)}
    | identifier as word {
        try
            let tkn = Hashtbl.find keywords word in
            logger#debug "Token found %s" word;
            tkn
        with Not_found ->
            logger#debug "Identifier found %s" word;
            ID(word)
    }
    | eof                { logger#debug "Finish source code flow"; EOF}
    | _   as unexpected  { Util.raise_lexer_error lexbuf (Printf.sprintf "Unexpected character: %c" unexpected) }
and comment = parse
  "*/"                   { logger#debug "Finish read a possible comment"; next_token lexbuf }
| _                      { comment lexbuf }
and singleline_comment = parse
  | next_line   { Lexing.new_line lexbuf; next_token lexbuf }
  | eof    { EOF }
  | _      { singleline_comment lexbuf }
