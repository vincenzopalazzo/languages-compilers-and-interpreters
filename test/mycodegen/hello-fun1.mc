// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int max_fun(int x, int y)
{
	int tmp;
	if (x < y)
		tmp = y;
	else
		tmp = x;
	return tmp;
}

int main()
{
	int max;
	max = max_fun(20, 11);
	print(max);
	return 0;
}
