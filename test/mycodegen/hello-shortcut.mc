// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int i;
	// Support assigment with i++
	// support declaration with int i = 0;
	for (i = 0; i < 20; i++) {
		print(i);
	}
	print(-1);
	i = 0;
	do {
		print(i++);
	} while (i < 20);

	int j = 0;
	for (i = 50; i < 70; i++)
		print(j);

	return 0;
}
