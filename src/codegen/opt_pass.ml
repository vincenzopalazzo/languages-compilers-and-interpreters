(* *
   * MicroC Compiler developer with University of Pisa for
   * courses Languages, Compilers and Interpreters class.
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
   * and all professors of the Languages, Compilers and Interpreters class
   * https://github.com/lillo/compiler-course-unipi
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *)

let optimize_module llvm_module =
  let pass_manager = Llvm.PassManager.create () in
  (* memory to register promontion *)
  Llvm_scalar_opts.add_memory_to_register_promotion pass_manager ;
  (* constant propagation *)
  Llvm_scalar_opts.add_constant_propagation pass_manager ;
  (* loop unrolling *)
  Llvm_scalar_opts.add_loop_unroll pass_manager ;
  (* dead code elimination *)
  Llvm_scalar_opts.add_aggressive_dce pass_manager ;
  (* control-flow graph simplification *)
  Llvm_scalar_opts.add_cfg_simplification pass_manager ;
  (* tail call elimination *)
  Llvm_scalar_opts.add_tail_call_elimination pass_manager ;
  Llvm.PassManager.run_module llvm_module pass_manager
