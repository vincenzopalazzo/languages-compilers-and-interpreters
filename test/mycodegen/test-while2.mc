// Author Vincenzo Palazzo v.palazzo1@studenti.unipit.iter

void increment(int *a) { (*a)++; }

int main()
{
	int a;
	a = 0;
	while (a < 20) {
		increment(&a);
		print(a);
	}
	return 0;
}
