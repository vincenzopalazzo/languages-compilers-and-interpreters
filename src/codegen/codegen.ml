(* *
   * MicroC Compiler developer with University of Pisa for
   * courses Languages, Compilers and Interpreters class.
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
   * and all professors of the Languages, Compilers and Interpreters class
   * https://github.com/lillo/compiler-course-unipi
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *)

open Ast
open Llvm
open Easy_logging

let logger = Logging.make_logger "CodeGen" Info [Cli Debug]

let llvm_variables = ref Llvmvalue_table.empty_table

let ( >-> ) key llval fun_param =
  logger#trace "Adding llvm global variable to map %s" key ;
  Llvmvalue_table.add_entry key llval !llvm_variables fun_param

let ( <-< ) key =
  logger#debug "Get llvm variable to map with %s" key ;
  Llvmvalue_table.lookup key !llvm_variables

let ( +-+ ) scope name_scope =
  llvm_variables := Llvmvalue_table.begin_block !scope name_scope ;
  ()

let ( -*- ) scope =
  llvm_variables := Llvmvalue_table.end_block !scope ;
  ()

let ( <*< ) scope = Llvmvalue_table.get_name_scope !scope

let llcontex = Llvm.global_context ()

let llvm_microc = Llvm.create_module llcontex "MicrocC-module"

(* MicroC types *)
let gen_int_type = Llvm.i32_type llcontex

let gen_bool_type = Llvm.i1_type llcontex

let gen_char_type = Llvm.i8_type llcontex

let gen_void_type = Llvm.void_type llcontex

(* TODO: refactoring *)
let () =
  ( >-> ) "print"
    (let print_type = Llvm.function_type gen_void_type [|gen_int_type|] in
     Llvm.declare_function "print" print_type llvm_microc )
    false

let () =
  ( >-> ) "getint"
    (let getint_type = Llvm.function_type gen_int_type [||] in
     Llvm.declare_function "getint" getint_type llvm_microc )
    false

let gen_array_type lltipe dim =
  match dim with
  | Some v -> Llvm.array_type lltipe v
  | None -> Llvm.array_type lltipe 1

let gen_pointer_type lltipe = Llvm.pointer_type lltipe

let rec to_llvm_type ast_type =
  match ast_type with
  | Ast.TypInt -> gen_int_type
  | Ast.TypBool -> gen_bool_type
  | Ast.TypChar -> gen_char_type
  | Ast.TypVoid -> gen_void_type
  | Ast.TypArray (tipe, dim) -> gen_array_type (to_llvm_type tipe) dim
  | Ast.TypPoint tipe -> gen_pointer_type (to_llvm_type tipe)
  | _ -> failwith "Unaspected tupe, this should never happen"

let contains s1 s2 =
  let re = Str.regexp_string s2 in
  try
    ignore (Str.search_forward re s1 0) ;
    true
  with Not_found -> false

let to_llvm_build_ret lltype builder =
  let typ_string = Llvm.string_of_lltype lltype in
  match contains typ_string "void" with
  | true -> Llvm.build_ret_void builder
  | _ ->
      let llval = Llvm.const_int lltype 0 in
      Llvm.build_ret llval builder

let add_terminal builder f =
  match Llvm.block_terminator (Llvm.insertion_block builder) with
  | Some _ -> ()
  | None -> ignore (f builder)

let rec translate_if_pointer llvm_val expr llvm_builder =
  match expr.node with
  | Ast.NullLiteral value -> (
      logger#debug "Processing NULL pointer ...." ;
      let tipe = Llvm.type_of llvm_val in
      let lltipe = Llvm.classify_type tipe in
      match lltipe with
      | Llvm.TypeKind.Integer | Llvm.TypeKind.Array ->
          Llvm.const_pointer_null (gen_pointer_type tipe)
      | Llvm.TypeKind.Pointer -> (
          let tipo_point = Llvm.element_type tipe in
          match Llvm.classify_type tipo_point with
          | Llvm.TypeKind.Pointer -> Llvm.const_pointer_null tipo_point
          | _ -> Llvm.const_pointer_null (gen_pointer_type tipo_point) ) 
      | _ -> failwith "Unsupported IR translating type" )
  | _ ->
      logger#debug "Not a pointer ...." ;
      translate_exp_to_llvm expr llvm_builder

and translate_exp_to_llvm exp_def llvm_builder =
  match exp_def.node with
  | Ast.ILiteral value ->
      logger#debug "Processing int type with value %d ...." value ;
      Llvm.const_int gen_int_type value
  | Ast.CLiteral value ->
      logger#debug "Processing char type ...." ;
      Llvm.const_int gen_char_type (int_of_char value)
  | Ast.BLiteral value ->
      logger#debug "Processing bool type ...." ;
      Llvm.const_int gen_bool_type (if value then 1 else 0)
  | Ast.Assign (variable, exp) ->
      logger#trace "*Assign stm* translating ..." ;
      let llvm_var = translate_acc_var variable llvm_builder in
      logger#debug "CG: variable to assign -> %s"
        (Llvm.string_of_llvalue llvm_var) ;
      let exp_to_assign = translate_if_pointer llvm_var exp llvm_builder in
      logger#debug "CG: Expression to assign -> %s"
        (Llvm.string_of_llvalue exp_to_assign) ;
      let _ = Llvm.build_store exp_to_assign llvm_var llvm_builder in
      logger#debug "CG: Assigment -> %s"
        (Llvm.string_of_llvalue exp_to_assign) ;
      exp_to_assign
  | Ast.DecAssig (dec, exp) ->
      let _ = translate_stm_to_llvm_ir llvm_builder dec in
      translate_exp_to_llvm exp llvm_builder
  | Ast.Access access ->
      logger#debug "*Access* stm translating ..." ;
      let var = translate_acc_var access llvm_builder in
      Llvm.build_load var "" llvm_builder
  | Ast.Call (id, exps) -> (
      logger#debug "*Call %s function translating ...*" id ;
      let llvm_exps =
        List.map
          (fun elem -> translate_fun_exp_to_llvm elem llvm_builder)
          exps
      in
      let func_dec = ( <-< ) id in
      let func_tipe = Llvm.type_of func_dec in
      let strin_type = Llvm.string_of_lltype func_tipe in
      match contains strin_type "void" with
      | true ->
          Llvm.build_call func_dec (Array.of_list llvm_exps) "" llvm_builder
      | _ ->
          Llvm.build_call func_dec (Array.of_list llvm_exps) id llvm_builder
      )
  | Ast.BinaryOp (op, exp1, exp2) -> (
      logger#debug "Binary Operation Translating ....." ;
      let first_exp = translate_exp_to_llvm exp1 llvm_builder in
      logger#debug "CG: First exp llvalue -> %s"
        (Llvm.string_of_llvalue first_exp) ;
      let second_exp = translate_if_pointer first_exp exp2 llvm_builder in
      logger#debug "CG: Second exp llvalue -> %s"
        (Llvm.string_of_llvalue second_exp) ;
      match op with
      | Add ->
          logger#debug "* + * Translating ..." ;
          let op = Llvm.build_add first_exp second_exp "" llvm_builder in
          logger#debug "CG: Binary operation llvalue -> %s"
            (Llvm.string_of_llvalue op) ;
          op
      | Sub ->
          logger#debug "* - * Translating ..." ;
          Llvm.build_sub first_exp second_exp "" llvm_builder
      | Mult ->
          logger#debug "* * *  Translating ...." ;
          Llvm.build_mul first_exp second_exp "" llvm_builder
      | Div ->
          logger#debug "* / * Translating ....." ;
          Llvm.build_sdiv first_exp second_exp "" llvm_builder
      | Mod ->
          logger#debug "* Mod * Translating ...." ;
          Llvm.build_srem first_exp second_exp "" llvm_builder
      | Equal ->
          logger#debug "* == * Translating .." ;
          Llvm.build_icmp Llvm.Icmp.Eq first_exp second_exp "" llvm_builder
      | Neq ->
          logger#debug "* != * Translating ..." ;
          Llvm.build_icmp Llvm.Icmp.Ne first_exp second_exp "" llvm_builder
      | Less ->
          logger#debug "* < * Translating ...." ;
          Llvm.build_icmp Llvm.Icmp.Slt first_exp second_exp "" llvm_builder
      | Leq ->
          logger#debug "* <= * Translating ...." ;
          Llvm.build_icmp Llvm.Icmp.Sle first_exp second_exp "" llvm_builder
      | Greater ->
          logger#debug "* > * Translating ...." ;
          Llvm.build_icmp Llvm.Icmp.Sgt first_exp second_exp "gt"
            llvm_builder
      | Geq ->
          logger#debug "* >= * Translating ..." ;
          Llvm.build_icmp Llvm.Icmp.Sge first_exp second_exp "ge"
            llvm_builder
      | And ->
          logger#debug "* && * Translating ..." ;
          Llvm.build_and first_exp second_exp "and" llvm_builder
      | Or ->
          logger#debug "* || * Translating ...." ;
          Llvm.build_or first_exp second_exp "or" llvm_builder
      | Comma ->
          logger#debug "TODO: analize , operator" ;
          failwith "TODO:" )
  | Ast.UnaryOp (op, exp) -> (
      logger#debug "* unary op * Translating ...." ;
      match op with
      | Ast.Neg ->
          let llv_ex = translate_exp_to_llvm exp llvm_builder in
          Llvm.build_neg llv_ex "" llvm_builder
      | Ast.Not ->
          let llv_ex = translate_exp_to_llvm exp llvm_builder in
          Llvm.build_not llv_ex "not" llvm_builder
      | Ast.PreInc ->
          let acc_var =
            match exp.node with
            | Access acc -> translate_acc_var acc llvm_builder
            | _ ->
                failwith
                  "Expression not valit for Pre and Post increment/decrement"
          in
          let llvm_var = acc_var in
          let llvm_load = Llvm.build_load llvm_var "" llvm_builder in
          let inc_value = Llvm.const_int gen_int_type 1 in
          let lladd = Llvm.build_add llvm_load inc_value "" llvm_builder in
          let _ = Llvm.build_store lladd llvm_var llvm_builder in
          lladd
      | Ast.PostInc ->
          let acc_var =
            match exp.node with
            | Access acc -> translate_acc_var acc llvm_builder
            | _ ->
                failwith
                  "Expression not valit for Pre and Post increment/decrement"
          in
          let llvm_var = acc_var in
          let llvm_load = Llvm.build_load llvm_var "" llvm_builder in
          let inc_value = Llvm.const_int gen_int_type 1 in
          let lladd = Llvm.build_add llvm_load inc_value "" llvm_builder in
          let _ = Llvm.build_store lladd llvm_var llvm_builder in
          llvm_load
      | Ast.PreDec ->
          let acc_var =
            match exp.node with
            | Access acc -> translate_acc_var acc llvm_builder
            | _ ->
                failwith
                  "Expression not valit for Pre and Post increment/decrement"
          in
          let llvm_var = acc_var in
          let llvm_load = Llvm.build_load llvm_var "" llvm_builder in
          let inc_value = Llvm.const_int gen_int_type 1 in
          let lladd = Llvm.build_sub llvm_load inc_value "" llvm_builder in
          let _ = Llvm.build_store lladd llvm_var llvm_builder in
          lladd
      | Ast.PostDec ->
          let acc_var =
            match exp.node with
            | Access acc -> translate_acc_var acc llvm_builder
            | _ ->
                failwith
                  "Expression not valit for Pre and Post increment/decrement"
          in
          let llvm_var = acc_var in
          let llvm_load = Llvm.build_load llvm_var "" llvm_builder in
          let inc_value = Llvm.const_int gen_int_type 1 in
          let lladd = Llvm.build_sub llvm_load inc_value "" llvm_builder in
          let _ = Llvm.build_store lladd llvm_var llvm_builder in
          llvm_load )
  | Ast.Addr access ->
      logger#debug "* &VAR* Translating ....." ;
      let llvar = translate_acc_var access llvm_builder in
      logger#debug "CG:Address varibale llvalue -> %s"
        (Llvm.string_of_llvalue llvar) ;
      llvar
  | _ -> failwith "Unvalid expression, this should never happen"

and translate_acc_var acc_def llvm_builder =
  match acc_def.node with
  | Ast.AccVar id -> (
      logger#trace "Access variable with id %s ...." id ;
      try ( <-< ) id
      with Not_found ->
        failwith
          "Variable not found (this error need to carch from sem. analysis)"
      )
  | AccIndex (acc, index) -> (
      let variable = translate_acc_var acc llvm_builder in
      let index_exp = translate_exp_to_llvm index llvm_builder in
      let zeros_pos = Llvm.const_int gen_int_type 0 in
      logger#trace "Variable -> %s" (Llvm.string_of_llvalue variable) ;
      logger#trace "index -> %s" (Llvm.string_of_llvalue index_exp) ;
      let const_index =
        Llvm.build_sext index_exp gen_int_type "" llvm_builder
      in
      logger#trace "const index -> %s" (Llvm.string_of_llvalue const_index) ;
      let variable_type = Llvm.type_of variable in
      match Llvm.classify_type variable_type with
      | Llvm.TypeKind.Pointer -> (
        match Llvm.classify_type (Llvm.element_type variable_type) with
        | Llvm.TypeKind.Array ->
            Llvm.build_in_bounds_gep variable
              [|zeros_pos; const_index|]
              "" llvm_builder
        | _ ->
            let load_val = Llvm.build_load variable "" llvm_builder in
            let pointer =
              Llvm.build_in_bounds_gep load_val [|const_index|] ""
                llvm_builder
            in
            logger#trace "Pointer -> %s" (Llvm.string_of_llvalue pointer) ;
            pointer )
      | _ ->
          let pointer =
            Llvm.build_in_bounds_gep variable
              [|zeros_pos; const_index|]
              "" llvm_builder
          in
          logger#trace "Array at -> %s" (Llvm.string_of_llvalue pointer) ;
          pointer )
  | AccDeref expr ->
      logger#debug "* *var * Translating ..." ;
      let llval = translate_exp_to_llvm expr llvm_builder in
      logger#debug "Access to pointer value -> %s"
        (Llvm.string_of_llvalue llval) ;
      llval

and translate_fun_exp_to_llvm exp llvm_builder =
  match exp.node with
  | Ast.Access acc -> translate_fun_acc_to_llvm acc llvm_builder
  | _ -> translate_exp_to_llvm exp llvm_builder

and translate_fun_acc_to_llvm acc llvm_builder =
  match acc.node with
  | Ast.AccVar id -> (
      let llval = ( <-< ) id in
      let lltyp = Llvm.type_of llval in
      let pos = Llvm.const_int gen_int_type 0 in
      match Llvm.classify_type lltyp with
      | Llvm.TypeKind.Array ->
          Llvm.build_in_bounds_gep llval [|pos; pos|] "" llvm_builder
      | Llvm.TypeKind.Pointer -> (
        match Llvm.classify_type (Llvm.element_type lltyp) with
        | Llvm.TypeKind.Array ->
            Llvm.build_in_bounds_gep llval [|pos; pos|] "" llvm_builder
        | _ -> Llvm.build_load llval "" llvm_builder )
      | _ -> Llvm.build_load llval "" llvm_builder )
  | _ ->
      let llval = translate_acc_var acc llvm_builder in
      Llvm.build_load llval "" llvm_builder

and translate_stm_to_llvm_ir llvm_builder stm_def =
  match stm_def.node with
  | Ast.Dec (tipe, id) -> (
      logger#debug "Declaration stm processing for type %s"
        (Ast.show_typ tipe) ;
      match tipe with
      | Ast.TypArray (arr_tipe, size) ->
          let llvm_arr = gen_array_type (to_llvm_type arr_tipe) size in
          let llvm_val = Llvm.build_alloca llvm_arr id llvm_builder in
          ( >-> ) id llvm_val false ; llvm_builder
      | _ ->
          logger#trace "Literal variable build with LLVM" ;
          let all_llvm =
            Llvm.build_alloca (to_llvm_type tipe) id llvm_builder
          in
          ( >-> ) id all_llvm false ; llvm_builder )
  | Ast.Stmt stmt -> translate_block_to_llvm stmt llvm_builder

and translate_block_to_llvm block_def llvm_builder =
  match block_def.node with
  | Ast.If (exp, stm1, stm2) ->
      let fun_def = ( <-< ) (( <*< ) llvm_variables) in
      logger#debug "* IF * Translating ..." ;
      let predicate = translate_exp_to_llvm exp llvm_builder in
      llvm_variables +-+ "if" ;
      let end_if = Llvm.append_block llcontex "end_if" fun_def in
      let if_body = Llvm.append_block llcontex "if" fun_def in
      let if_builder = Llvm.builder_at_end llcontex if_body in
      let end_if_builder = translate_block_to_llvm stm1 if_builder in
      add_terminal end_if_builder (Llvm.build_br end_if) ;
      let else_body = Llvm.append_block llcontex "else" fun_def in
      let else_builder = Llvm.builder_at_end llcontex else_body in
      let end_else_builder = translate_block_to_llvm stm2 else_builder in
      add_terminal end_else_builder (Llvm.build_br end_if) ;
      let _ = Llvm.build_cond_br predicate if_body else_body llvm_builder in
      Llvm.builder_at_end llcontex end_if
  | Ast.While (exp1, stm) ->
      logger#debug "* While * Translating ..." ;
      let fun_name = ( <*< ) llvm_variables in
      logger#debug "Function name is %s" fun_name ;
      let fun_def = ( <-< ) fun_name in
      let while_header = Llvm.append_block llcontex "while" fun_def in
      let _ = Llvm.build_br while_header llvm_builder in
      let while_body = Llvm.append_block llcontex "while_body" fun_def in
      let body_builder = Llvm.builder_at_end llcontex while_body in
      llvm_variables +-+ "while" ;
      let from_body_builder = translate_block_to_llvm stm body_builder in
      add_terminal from_body_builder (Llvm.build_br while_header) ;
      let header_builder = Llvm.builder_at_end llcontex while_header in
      ( -*- ) llvm_variables ;
      let condition = translate_exp_to_llvm exp1 header_builder in
      let end_while = Llvm.append_block llcontex "end_while" fun_def in
      let _ =
        Llvm.build_cond_br condition while_body end_while header_builder
      in
      Llvm.builder_at_end llcontex end_while
  | Ast.DoWhile (exp1, stmt) ->
      logger#trace "* Do While * Translating ...." ;
      let fun_name = ( <*< ) llvm_variables in
      logger#debug "Function name is: %s" fun_name ;
      let fun_def = ( <-< ) fun_name in
      llvm_variables +-+ "do-while" ;
      let _ = translate_block_to_llvm stmt llvm_builder in
      let do_header = Llvm.append_block llcontex "do-while" fun_def in
      let do_body = Llvm.append_block llcontex "do-body" fun_def in
      let do_end = Llvm.append_block llcontex "do-end" fun_def in
      let _ = Llvm.build_br do_header llvm_builder in
      Llvm.position_at_end do_header llvm_builder ;
      let condition = translate_exp_to_llvm exp1 llvm_builder in
      ( -*- ) llvm_variables ;
      let _ = Llvm.build_cond_br condition do_body do_end llvm_builder in
      Llvm.position_at_end do_body llvm_builder ;
      let last_builder = translate_block_to_llvm stmt llvm_builder in
      add_terminal last_builder (Llvm.build_br do_header) ;
      Llvm.builder_at_end llcontex do_end
  | Ast.Expr ex ->
      logger#debug "*Exp declaration* processing ..." ;
      let _ = translate_exp_to_llvm ex llvm_builder in
      llvm_builder
  | Ast.Return exp -> (
      logger#debug "*Return stm* processing ...." ;
      Llvmvalue_table.set_end_block !llvm_variables true ;
      match exp with
      | Some v ->
          let return_val = translate_exp_to_llvm v llvm_builder in
          let _ = Llvm.build_ret return_val llvm_builder in
          llvm_builder
      | None ->
          logger#debug "Return void stm ...." ;
          let _ = Llvm.build_ret_void llvm_builder in
          llvm_builder )
  | Ast.Block declarations ->
      logger#debug "*Processing block .....*" ;
      let rec iterate declarations llvm_builder =
        match declarations with
        | [] -> llvm_builder
        | x :: xs ->
            let is_finished = Llvmvalue_table.is_ended !llvm_variables in
            let is_fun = Llvmvalue_table.is_function !llvm_variables in
            logger#debug "dec in block" ;
            logger#debug "With value value %s"
              (if is_finished then "true" else "false") ;
            if is_finished && is_fun then llvm_builder
            else
              let new_builder = translate_stm_to_llvm_ir llvm_builder x in
              iterate xs new_builder
      in
      iterate declarations llvm_builder

let manage_function_params list_params llvm_builder fun_dec =
  let fun_params = Llvm.params fun_dec in
  List.iteri
    (fun index (tipe, id) ->
      match tipe with
      | Ast.TypArray (t, size) ->
          let pointer_type = gen_pointer_type (to_llvm_type t) in
          let local_par = Llvm.build_alloca pointer_type id llvm_builder in
          let _ =
            Llvm.build_store fun_params.(index) local_par llvm_builder
          in
          ( >-> ) id local_par true
      | _ ->
          let local_par =
            Llvm.build_alloca (to_llvm_type tipe) id llvm_builder
          in
          let _ =
            Llvm.build_store fun_params.(index) local_par llvm_builder
          in
          ( >-> ) id local_par true )
    list_params

let unwrap_option_exp exp_node llvm_builder =
  match exp_node with
  | Some v -> ignore (translate_exp_to_llvm v llvm_builder)
  | None -> ()

let translate_function_dec func_def =
  match func_def with
  | func_def as f ->
      logger#debug "*Function declaration* Function %s loading ..." f.fname ;
      let return_type = to_llvm_type f.typ in
      let params_type =
        Array.of_list
          (List.map
             (fun (tipe, _) ->
               match tipe with
               (* An array is passed by reference, in particular by reference
                  to the first element, as C style *)
               | TypArray (t, size) -> Llvm.pointer_type (to_llvm_type t)
               | _ -> to_llvm_type tipe )
             f.formals )
      in
      let funtype = Llvm.function_type return_type params_type in
      let fundef = Llvm.define_function f.fname funtype llvm_microc in
      ( >-> ) f.fname fundef false ;
      let builder = Llvm.builder_at_end llcontex (Llvm.entry_block fundef) in
      llvm_variables +-+ f.fname ;
      manage_function_params f.formals builder fundef ;
      let last_builder = translate_block_to_llvm f.body builder in
      add_terminal last_builder (to_llvm_build_ret funtype) ;
      ( -*- ) llvm_variables

let rec get_def_val_by_tipe ast_type =
  match ast_type with
  | Ast.TypInt | Ast.TypChar | Ast.TypBool ->
      Llvm.const_int (to_llvm_type ast_type) 0
  | Ast.TypArray (t, size) -> (
    match size with
    | Some v ->
        let llsize = Llvm.const_int gen_int_type v in
        Llvm.const_array (to_llvm_type t) [|llsize|]
    | None ->
        let llsize = Llvm.const_int gen_int_type 0 in
        Llvm.const_array (to_llvm_type t) [|llsize|] )
  | Ast.TypPoint tipe ->
      Llvm.const_pointer_null (gen_pointer_type (to_llvm_type tipe))
  | _ -> failwith "ERROR GET DEFAULT VALUE TYPE"

let rec define_variable_with_val ast_node tipe_expr =
  match ast_node.node with
  | Ast.ILiteral value -> 
      logger#debug
        "*Global variable with assignment* LLVM IR Translating...." ;
      Llvm.const_int gen_int_type value
  | Ast.CLiteral value ->
      logger#debug
        "*Global variable with assignment* LLVM IR Translating...." ;
      Llvm.const_int gen_char_type (int_of_char value)
  | Ast.BLiteral value ->
      logger#debug
        "*Global variable with assignment* LLVM IR Translating...." ;
      Llvm.const_int gen_bool_type (if value then 1 else 0)
  | Ast.NullLiteral _ ->
    logger#debug "*Global NULL definition* LLVM IR translating ...." ;
    Llvm.const_pointer_null tipe_expr
  | Ast.UnaryOp (op, exp) -> (
    logger#debug "*Globlal uniary op* LLVM IR translating ....." ;
    let const_exp = define_variable_with_val exp tipe_expr in
    match op with
    | Ast.Neg ->
      Llvm.const_neg const_exp 
    | Ast.Not ->
      Llvm.const_not const_exp
    | _ -> failwith "TODO: not supported" )
  | Ast.BinaryOp (bop, exp1, exp2) -> (
    logger#debug "* Global Binary OP * LLVM IR translating ...." ;
    let llval_one = define_variable_with_val exp1 tipe_expr in
    let llval_two = define_variable_with_val exp2 tipe_expr in
    match bop with
    | Ast.Add ->
      Llvm.const_add llval_one llval_two
    | Ast.Sub ->
      Llvm.const_sub llval_one llval_two
    | Ast.Mult ->
      Llvm.const_mul llval_one llval_two
    | Ast.Div ->
      Llvm.const_sdiv llval_one llval_two
    | Ast.Mod ->
      Llvm.const_srem llval_one llval_two
    | Ast.Equal ->
      Llvm.const_icmp Llvm.Icmp.Eq llval_one llval_two
    | Ast.Neq ->
      Llvm.const_icmp Llvm.Icmp.Ne llval_one llval_two
    | Ast.Less ->
      Llvm.const_icmp Llvm.Icmp.Slt llval_one llval_two
    | Ast.Leq ->
      Llvm.const_icmp Llvm.Icmp.Sle llval_one llval_two
    | Ast.Greater ->
      Llvm.const_icmp Llvm.Icmp.Sgt llval_one llval_two
    | Ast.Geq ->
      Llvm.const_icmp Llvm.Icmp.Sge llval_one llval_two
    | Ast.And ->
      Llvm.const_and llval_one llval_two
    | Ast.Or ->
      Llvm.const_or llval_one llval_two
    | _ -> failwith "Unsupported Binary operation"
    )
  | _ -> failwith "Unsupported assigment expression"

let translate_global_dec ast_topdec =
  match ast_topdec.node with
  | Ast.Vardec (tipe, id) ->
      logger#debug "*Global Variable* LLVM IR coming ..." ;
      let init_val = get_def_val_by_tipe tipe in
      let llvm_val = Llvm.define_global id init_val llvm_microc in
      ( >-> ) id llvm_val false
  | Ast.DecAssign (dec, exp) -> (
    match dec.node with
    | Ast.Vardec (tipe, id) ->
        let llvm_val = define_variable_with_val exp (to_llvm_type tipe) in
        let llvm_const = Llvm.define_global id llvm_val llvm_microc in
        ( >-> ) id llvm_const false
    | _ -> failwith "TODO usupported declaration" )
  | Ast.Fundecl fundec ->
      logger#debug "*Function declaration* LLVM IR coming..." ;
      translate_function_dec fundec

let to_ir (Prog topdecls) =
  List.iter translate_global_dec topdecls ;
  llvm_microc
