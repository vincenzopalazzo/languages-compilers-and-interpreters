"""
MicroC Compiler developer with University of Pisa for
courses Languages, Compilers and Interpreters class.
Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
and all professors of the Languages, Compilers and Interpreters class
https://github.com/lillo/compiler-course-unipi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
#!/usr/bin/env python3

import subprocess
import os
import glob
import logging
import pytest
import cpuinfo

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s | %(levelname)s | %(message)s", "%m-%d-%Y %H:%M:%S"
)

CPU_INFO = cpuinfo.get_cpu_info()

LLVM_TARGET = {
    "X86_64": "x86-64",
    "X86_32": "x86",
    "ARM_7": "arm"
}

def test_mycodegen_success_compilation():
    """
    This test case use run the microc compiler with
    the file to have a compilation success.
    """
    count_tests = 0
    for filepath in glob.iglob("test/mycodegen/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", filepath])
        assert output is 0, "T: {} Error on file {}".format(count_tests, filepath)


def test_mycodegen_failure_compilation():
    """
    This test case use run the microc compiler with
    the file to have a compilation failure .
    """
    count_tests = 0
    for file_path in glob.iglob("test/mycodegen/fail-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", file_path])
        assert output is not 0, "T: {} Error on file {}".format(count_tests, file_path)

def test_codegen_success_compilation():
    """
    This test case use run the microc compiler with
    the file to have a compilation success.
    """
    count_tests = 0
    for filepath in glob.iglob("test/codegen/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", filepath])
        assert output is 0, "T: {} Error on file {}".format(count_tests, filepath)


def test_codegen_failure_compilation():
    """
    This test case use run the microc compiler with
    the file to have a compilation failure .
    """
    count_tests = 0
    for file_path in glob.iglob("test/codegen/fail-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", file_path])
        assert output is not 0, "T: {} Error on file {}".format(count_tests, file_path)


def test_success_compilation_semantic():
    """
    This test case use run the microc compiler with
    the file to have a compilation success on semantic check.
    """
    count_tests = 0
    for filepath in glob.iglob("test/semantic/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-s", filepath])
        assert output is 0, "T: {} Error on file {}".format(count_tests, filepath)


def test_failure_compilation_semantic():
    """
    This test case use run the microc compiler with
    the file to have a compilation failure on semantic check.
    """
    count_tests = 0
    for file_path in glob.iglob("test/semantic/fail-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-s", file_path])
        assert output is not 0, "T: {} Error on file {}".format(count_tests, file_path)


def test_success_compilation_parser():
    """
    This test case use run the microc compiler with
    the file to have a compilation success on parsing.
    """
    count_tests = 0
    for filepath in glob.iglob("test/parser/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-p", filepath])
        assert output is 0, "T: {} Error on file {}".format(count_tests, filepath)


def test_failure_compilation_parser():
    """
    This test case use run the microc compiler with
    the file to have a compilation failure on parsing.
    """
    count_tests = 0
    for file_path in glob.iglob("test/parser/fail-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-p", file_path])
        assert output is not 0, "T: {} Error on file {}".format(count_tests, file_path)


def test_my_gen_code_hello():
    """
    This test case use run the microc compiler with
    the file to have a compilation failure on parsing.
    """
    count_tests = 0
    for file_path in glob.iglob("test/mycodegen/hello-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", file_path])
        assert output is 0, "T: {} Error on file {}".format(count_tests, file_path)


def test_codegen_llvm_no_llvm_optimizzation():
    """
    Test case that contains the logic to run compiler microc and link the sdk
    to make the file runnable,
    In addition, this file contains also the logic to check the diff with the
    .out files.
    """
    output = subprocess.call(["clang-10", "-c", "-Wall", "sdk/rt-support.c"])
    assert output is 0, "Build SDK Failing"
    count_tests = 0
    for file_path in glob.iglob("test/codegen/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-c", file_path])
        llvm_tar = LLVM_TARGET[CPU_INFO["arch"]]
        output = subprocess.call(["llc-10", "-march={}".format(llvm_tar),  "-relocation-model=pic", "a.bc"])
        assert output is 0, "T: {} Build LLVM Failing on file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["clang-10", "a.s", "rt-support.o", "-lm"])
        assert (
            output is 0
        ), "T: {} Linking file + sdk with CLANG Failing on file {}".format(
            count_tests, file_path
        )
        name_file = file_path.split(".")[0]
        logger.debug("Name file {}".format(name_file))
        try:
            result = subprocess.check_output(["./a.out"])
        except subprocess.CalledProcessError as exc:
            result = exc.output
        there_is_diff = compare("{}.out".format(name_file), result)
        assert there_is_diff is True, "T: {} Different result in file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["rm", "-f", "*.o", " *.s", "*.bc"])
        assert output is 0, "T: {} Clean directory failed on file {}".format(
            count_tests, file_path
        )

def test_codegen_llvm():
    """
    Thest case tha contains the logic to run compiler microc and link the sdk
    to make the file runnable,
    In addition, this file contains also the logic to check the diff with the
    .out files.
    """
    output = subprocess.call(["clang-10", "-c", "-Wall", "sdk/rt-support.c"])
    assert output is 0, "Build SDK Failing"
    count_tests = 0
    for file_path in glob.iglob("test/codegen/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-c", "-O", file_path])
        llvm_tar = LLVM_TARGET[CPU_INFO["arch"]]
        output = subprocess.call(["llc-10", "-march={}".format(llvm_tar), "-relocation-model=pic", "a.bc"])
        assert output is 0, "T: {} Build LLVM Failing on file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["clang-10", "a.s", "rt-support.o", "-lm"])
        assert (
            output is 0
        ), "T: {} Linking file + sdk with CLANG Failing on file {}".format(
            count_tests, file_path
        )
        name_file = file_path.split(".")[0]
        logger.debug("Name file {}".format(name_file))
        try:
            result = subprocess.check_output(["./a.out"])
        except subprocess.CalledProcessError as exc:
            result = exc.output
        there_is_diff = compare("{}.out".format(name_file), result)
        assert there_is_diff is True, "T: {} Different result in file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["rm", "-f", "*.o", " *.s", "*.bc"])
        assert output is 0, "T: {} Clean directory failed on file {}".format(
            count_tests, file_path
        )

def test_mycodegen_llvm_no_llvm_optimizzation():
    """
    Test case that contains the logic to run compiler microc and link the sdk
    to make the file runnable,
    In addition, this file contains also the logic to check the diff with the
    .out files.
    """
    output = subprocess.call(["clang-10", "-c", "-Wall", "sdk/rt-support.c"])
    assert output is 0, "Build SDK Failing"
    count_tests = 0
    for file_path in glob.iglob("test/mycodegen/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-c", file_path])
        llvm_tar = LLVM_TARGET[CPU_INFO["arch"]]
        output = subprocess.call(["llc-10", "-march={}".format(llvm_tar),  "-relocation-model=pic", "a.bc"])
        assert output is 0, "T: {} Build LLVM Failing on file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["clang-10", "a.s", "rt-support.o", "-lm"])
        assert (
            output is 0
        ), "T: {} Linking file + sdk with CLANG Failing on file {}".format(
            count_tests, file_path
        )
        name_file = file_path.split(".")[0]
        logger.debug("Name file {}".format(name_file))
        try:
            result = subprocess.check_output(["./a.out"])
        except subprocess.CalledProcessError as exc:
            result = exc.output
        there_is_diff = compare("{}.out".format(name_file), result)
        assert there_is_diff is True, "T: {} Different result in file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["rm", "-f", "*.o", " *.s", "*.bc"])
        assert output is 0, "T: {} Clean directory failed on file {}".format(
            count_tests, file_path
        )


def test_mycodegen_llvm():
    """
    Thest case tha contains the logic to run compiler microc and link the sdk
    to make the file runnable,
    In addition, this file contains also the logic to check the diff with the
    .out files.
    """
    output = subprocess.call(["clang-10", "-c", "-Wall", "sdk/rt-support.c"])
    assert output is 0, "Build SDK Failing"
    count_tests = 0
    for file_path in glob.iglob("test/mycodegen/test-*.mc"):
        count_tests = count_tests + 1
        output = subprocess.call(["./microcc", "-c", "-O", file_path])
        llvm_tar = LLVM_TARGET[CPU_INFO["arch"]]
        output = subprocess.call(["llc-10", "-march={}".format(llvm_tar), "-relocation-model=pic", "a.bc"])
        assert output is 0, "T: {} Build LLVM Failing on file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["clang-10", "a.s", "rt-support.o", "-lm"])
        assert (
            output is 0
        ), "T: {} Linking file + sdk with CLANG Failing on file {}".format(
            count_tests, file_path
        )
        name_file = file_path.split(".")[0]
        logger.debug("Name file {}".format(name_file))
        try:
            result = subprocess.check_output(["./a.out"])
        except subprocess.CalledProcessError as exc:
            result = exc.output
        there_is_diff = compare("{}.out".format(name_file), result)
        assert there_is_diff is True, "T: {} Different result in file {}".format(
            count_tests, file_path
        )

        output = subprocess.call(["rm", "-f", "*.o", " *.s", "*.bc"])
        assert output is 0, "T: {} Clean directory failed on file {}".format(
            count_tests, file_path
        )

def compare(expected, actual):
    """
    Make the comparison with the expected file .out in the
    directory test with the context recived by the exectution
    of the runnable file compiler with microc
    :expected it is the name of the file that contains the corrct result
    :actual bytes string with the resul of the MicrocC program
    """
    try:
        with open(expected, "r") as f:
            expected_lines = f.readlines()
    except:
        logger.warning("Minsing result file {}".format(expected))
        return True
    actual_lines = actual.splitlines()
    if len(expected_lines) != len(actual_lines):
        return False
    for i in range(len(expected_lines)):
        expected_line = expected_lines[i].strip()
        actual_line = actual_lines[i].decode("utf-8").strip()
        if expected_line != actual_line:
            logger.critical("{} different from {}".format(expected_line, actual_line))
            return False
    return True
