// First add program with MicroC and Llvm
// author v.palazzo1@studenti.unipi.it

int main()
{
	int a;
	// Binary operation wihout accessing
	a = 12 + 1;
	a = 23 - 2;
	a = 23 / 1;
	a = 12 * 2;
	// Binary operation with accessing
	a = a - 2;
	print(a);
	return 0;
}
