// Author Vincenzo Palazzo v.palazzo@stundeti.unipi.it

int max(int *x, int *y)
{
	if (x == NULL || y == NULL) {
		return -1;
	}
	print(1);
	if (*x < *y)
		return *y;
	return *x;
}

int main()
{
	int *x;
	x = NULL;
	int *y;
	y = NULL;
	int a = 1;
	y = &a;
	a = max(x, y);
	print(a);

	x = &a;
	a = max(x, y);
	print(a);
	return 0;
}