"""
MicroC Compiler developer with University of Pisa for
courses Languages, Compilers and Interpreters class.
Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
and all professors of the Languages, Compilers and Interpreters class
https://github.com/lillo/compiler-course-unipi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
#!/usr/bin/env python3

import subprocess
import os
import glob
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s | %(levelname)s | %(message)s", "%m-%d-%Y %H:%M:%S"
)


def _clean_def_test_dir():
    """
    This script is an helper script to run setup the 
    direcotry test and clean the test directory codegen.

    This script run over the test script and relocate the 
    file in the correct direcory.
    """
    logger.info("Start filtering default tests from custom tests")
    try:
        for filepath in glob.iglob("test/codegen/test-*.mc"):
            split_list = filepath.split("/")
            name_file = "{}/{}".format(split_list[0], split_list[2])
            if (os.path.isfile(name_file)):
                os.remove(name_file)
                logger.info("File {} removed".format(name_file))

        for filepath in glob.iglob("test/codegen/test-*.out"):
            split_list = filepath.split("/")
            name_file = "{}/{}".format(split_list[0], split_list[2])
            if (os.path.isfile(name_file)):
                os.remove(name_file)
                logger.info("File {} removed".format(name_file))

        for filepath in glob.iglob("test/codegen/test-*.mc"):
            split_list = filepath.split("/")
            name_file = "{}/mycodegen/{}".format(split_list[0], split_list[2])
            if (os.path.isfile(name_file)):
                os.remove(name_file)
                logger.info("File {} removed".format(name_file))

        for filepath in glob.iglob("test/codegen/test-*.out"):
            split_list = filepath.split("/")
            name_file = "{}/mycodegen/{}".format(split_list[0], split_list[2])
            if (os.path.isfile(name_file)):
                os.remove(name_file)
                logger.info("File {} removed".format(name_file))
        
        for filepath in glob.iglob("test/codegen/fail-*.mc"):
            split_list = filepath.split("/")
            name_file = "{}/{}".format(split_list[0], split_list[2])
            if (os.path.isfile(name_file)):
                os.remove(name_file)
                logger.info("File {} removed".format(name_file))

        for filepath in glob.iglob("test/codegen/fail-*.mc"):
            split_list = filepath.split("/")
            name_file = "{}/mycodegen/{}".format(split_list[0], split_list[2])
            if (os.path.isfile(name_file)):
                os.remove(name_file)
                logger.info("File {} removed".format(name_file))
    except:
        logger.info("File {} Skipped".format(filepath))

    logger.info("Start Copy personal tests")
    for filepath in glob.iglob("test/test-*.*"):
        split_list = filepath.split("/")
        name_file = "{}/mycodegen/{}".format(split_list[0], split_list[1])
        os.rename(filepath, name_file)
        logger.info("Copy {} in {}".format(filepath, name_file))

    for filepath in glob.iglob("test/fail-*.*"):
        split_list = filepath.split("/")
        name_file = "{}/mycodegen/{}".format(split_list[0], split_list[1])
        os.rename(filepath, name_file)
        logger.info("Copy {} in {}".format(filepath, name_file))
    
    for filepath in glob.iglob("test/hello-*.*"):
        split_list = filepath.split("/")
        name_file = "{}/mycodegen/{}".format(split_list[0], split_list[1])
        os.rename(filepath, name_file)
        logger.info("Copy {} in {}".format(filepath, name_file))


if __name__ == '__main__':
    print(
        """
----------------------------------------------------------------------
MicroC Compiler developer with University of Pisa for
courses Languages, Compilers and Interpreters class.
Copyright (C) 2020-2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
and all professors of the Languages, Compilers and Interpreters class
https://github.com/lillo/compiler-course-unipi
----------------------------------------------------------------------
        """)
    logging.basicConfig(level=logging.INFO)
    _clean_def_test_dir()