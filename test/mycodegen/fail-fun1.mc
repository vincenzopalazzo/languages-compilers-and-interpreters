// author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int increment(int a) { return a + 1; }

int main()
{
	char i;
	i = 'c';
	print(increment(i));
	print(i = i + 1);
	return 0;
}
