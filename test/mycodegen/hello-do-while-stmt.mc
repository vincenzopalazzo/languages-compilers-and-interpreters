// author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int a;
	a = 0;
	do {
		a = a + 1;
		print(a);
	} while (a < 10);
	return 0;
}
