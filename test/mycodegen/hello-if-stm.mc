// Author: Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int a;
	a = getint();
	if (a == 1) {
		print(a);
		return 0;
	} else {
		print(-1);
		return -1;
	}
	return 0;
}
