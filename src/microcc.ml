(* *
   * MicroC Compiler developer with University of Pisa for 
   * courses Languages, Compilers and Interpreters class. 
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com 
   * and all professors of the Languages, Compilers and Interpreters class 
   * https://github.com/lillo/compiler-course-unipi 
   *
   * This program is free software; you can redistribute it and/or 
   * modify it under the terms of the GNU General Public License 
   * as published by the Free Software Foundation; either version 2 
   * of the License, or (at your option) any later version. 
   * 
   * This program is distributed in the hope that it will be useful, 
   * but WITHOUT ANY WARRANTY; without even the implied warranty of 
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
   * GNU General Public License for more details. 
   * 
   * You should have received a copy of the GNU General Public License 
   * along with this program; if not, write to the Free Software 
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. 
  *)
open Ast

type action = Dump_ast | Semantic_check | Dump_llvm_ir | Compile

let () =
  let action = ref Compile in
  let filename = ref "" in
  let outputfile = ref "a.bc" in
  let optimize = ref false in
  let spec_list =
    [ ("-p", Arg.Unit (fun () -> action := Dump_ast), "Parse and print AST")
    ; ( "-s"
      , Arg.Unit (fun () -> action := Semantic_check)
      , "Perform semantic checks" )
    ; ( "-d"
      , Arg.Unit (fun () -> action := Dump_llvm_ir)
      , "Print the generated code" )
    ; ( "-o"
      , Arg.String (fun name -> outputfile := name)
      , "Place the output into file" )
    ; ("-O", Arg.Set optimize, "Optimize the generated code")
    ; ("-c", Arg.Unit (fun () -> action := Compile), "Compile (default)") ]
  in
  let usage =
    Printf.sprintf "Usage:\t%s [-p|-s|-d|-c] <source_file>\n" Sys.argv.(0)
  in
  Arg.parse spec_list (fun file -> filename := file) usage ;
  let in_channel = if !filename = "" then stdin else open_in !filename in
  let lexbuf = Lexing.from_channel in_channel in
  try
    let (Prog ds as p) = Parser_engine.parse lexbuf in
    if !action = Dump_ast then (
      Printf.printf "Number of found declarations: %d\n" (List.length ds) ;
      Printf.printf "Ast dump\n %s\n" (Ast.show_program p) )
    else
      let _ = Semant.check p in
      if !action = Semantic_check then
        Printf.printf "Semantic checks: pass\n"
      else
        let llvm_module = Codegen.to_ir p in
        if !optimize then Opt_pass.optimize_module llvm_module |> ignore ;
        if !action = Dump_llvm_ir then
          print_string (Llvm.string_of_llmodule llvm_module)
        else if
          not (Llvm_bitwriter.write_bitcode_file llvm_module !outputfile)
        then
          Printf.fprintf stderr "An error occured when generating the file"
  with
  | Util.Lexing_error m ->
      Printf.fprintf stderr "Lexing error: %s:%s\n" !filename m ;
      exit 1
  | Util.Syntax_error m ->
      Printf.fprintf stderr "Syntax error: %s:%s\n" !filename m ;
      exit 2
  | Util.Semantic_error m ->
      Printf.fprintf stderr "Error: %s:%s\n" !filename m ;
      exit 3
