// Author Vincenzo Palazzo v.palazzo@stundeti.unipi.it

int max(char *x, char *y)
{
	if (x == NULL || y == NULL) {
		return -1;
	}
	if (*x == *y)
		return 1;
	return 0;
}

int main()
{
	print(max(NULL, NULL));
	print(max(NULL, NULL));
	return 0;
}