// author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
#include <stdio.h>

int main()
{
	int x;
	int a;

	x = 10;
	a = x++;

	printf("%d\n", a);
	printf("%d\n", x);
	return 0;
}
