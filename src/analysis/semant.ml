(**
  * MicroC Compiler developer with University of Pisa for
  * courses Languages, Compilers and Interpreters class.
  *
  * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
  * and all professors of the Languages, Compilers and Interpreters class
  * https://github.com/lillo/compiler-course-unipi
  *
  * This program is free software; you can redistribute it and/or
  * modify it under the terms of the GNU General Public License
  * as published by the Free Software Foundation; either version 2
  * of the License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program; if not, write to the Free Software
  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*)

open Ast
open Symbol_table
open Easy_logging
open Util

let logger = Logging.make_logger "Semant" Info [Cli Debug]

let fun_definition : (string, (int, Ast.typ) Hashtbl.t) Hashtbl.t =
  Hashtbl.create 0

let ( >-> ) fun_name index type_par loc =
  try (
  let elem = Hashtbl.find_opt fun_definition fun_name in
  match elem with
  | Some v -> Hashtbl.add v index type_par
  | None ->
      Hashtbl.add fun_definition fun_name (Hashtbl.create 0) ;
      let parmas = Hashtbl.find fun_definition fun_name in
      Hashtbl.add parmas index type_par
  ) with Not_found -> (
    logger#debug "Function %s not in the scope" fun_name ;
      Util.raise_semantic_error loc
        (Printf.sprintf "Function %s not in the scope" fun_name)
  )

let ( >? ) fun_name pos =
  logger#debug "Fun name %s -> pos %d " fun_name pos ;
  let params = Hashtbl.find fun_definition fun_name in
  Hashtbl.find params pos

let global_scope = ref Symbol_table.empty_table

let () =
  Symbol_table.add_entry "print" Ast.TypVoid !global_scope ;
  ( >-> ) "print" 0 Ast.TypInt Ast.dummy_pos

let () = Symbol_table.add_entry "getint" Ast.TypInt !global_scope

let unwrap_pointer ast_pointer =
  match ast_pointer with Ast.TypPoint tipe -> tipe | _ -> ast_pointer

let check_return_type type_def ast_node =
  match type_def with
  | Ast.TypInt | Ast.TypBool | Ast.TypChar -> ()
  | _ ->
      logger#debug "Type %s not allowed in return stm"
        (Util.convert_type_to_str type_def) ;
      Util.raise_semantic_error ast_node.loc
        (Printf.sprintf "Type %s not allowed in return stm"
           (Util.convert_type_to_str type_def) )

let check_var_type_allowed type_def ast_node =
  match type_def with
  | Ast.TypInt | Ast.TypBool | Ast.TypChar
   |Ast.TypArray (_, _)
   |Ast.TypPoint _ ->
      ()
  | _ ->
      logger#debug "Type %s not allowed" (Util.convert_type_to_str type_def) ;
      Util.raise_semantic_error ast_node.loc
        (Printf.sprintf "Type %s not allowed "
           (Util.convert_type_to_str type_def) )

let rec check_basic_type typ_def =
  match typ_def with
  | Ast.TypInt | Ast.TypChar | Ast.TypBool -> true
  | _ -> false

and check_same_basic_type typ_def_one typ_def_two =
  if check_basic_type typ_def_one && check_basic_type typ_def_two then
    if typ_def_one = Ast.TypInt && typ_def_two = Ast.TypInt then true
    else if typ_def_one = Ast.TypChar && typ_def_two = Ast.TypChar then true
    else if typ_def_one = Ast.TypBool && typ_def_two = Ast.TypBool then true
    else false
  else false

let check_function_type_allowed type_def =
  match type_def with
  | Ast.TypInt | Ast.TypBool | Ast.TypChar | Ast.TypVoid -> ()
  | _ -> failwith "Function type not allowed"

let check_arry_type type_dec ast_node =
  match type_dec with
  | Ast.TypArray (tipe, dim) -> (
    match dim with
    | Some v ->
        if v < 1 then
          Util.raise_semantic_error ast_node.loc
            (Printf.sprintf "Size array not allowed, %d is an invalid size" v)
        else ()
    | None ->
        Util.raise_semantic_error ast_node.loc
          (Printf.sprintf "Size Array invalid, minimum size is 1") )
  | _ -> ()

let rec check_mem_acc node_one node_two =
  match node_one with
  | Ast.TypPoint tipe_one -> (
    match node_two.node with
    | Ast.Addr acc -> check_acc acc
    | _ ->
        let tipe_two = check_exp node_two in
        if node_one <> tipe_two && tipe_two <> TypPoint TypNull then
          Util.raise_semantic_error node_two.loc
            (Printf.sprintf
               "Type %s it is a pointer and it is no not compatible with %s"
               (Util.convert_type_to_str node_one)
               (Util.convert_type_to_str (check_exp node_two)) )
        else tipe_two )
  | _ -> (
    match node_two.node with
    | Access acc -> (
      match acc.node with
      | Ast.AccDeref _ | AccIndex _ -> check_exp node_two
      | _ ->
          logger#debug "Type Mismetch %s is different from what is expected %s"
          (Util.convert_type_to_str node_one)
          (Util.convert_type_to_str (check_exp node_two)) ;
            Util.raise_semantic_error node_two.loc
            (Printf.sprintf
              "Type Mismetch %s is different from what is expected %s"
               (Util.convert_type_to_str (check_exp node_two))
               (Util.convert_type_to_str node_one )) 
      )
    | _ ->
        logger#debug "Unrecognize type" ;
        Util.raise_semantic_error node_two.loc
          (Printf.sprintf "Unrecognize type %s" (Util.convert_type_to_str node_one)) 
    )

and check_acc acc =
  match acc.node with
  | AccVar id -> (
    try (
      logger#debug "Access variable check" ;
      let var_type = Symbol_table.lookup id !global_scope in
      logger#trace "Variable found with id %s and type %s" id
        (show_typ var_type) ;
      var_type
    ) with NotFoundInScope -> (
      logger#debug "Variable %s not found" id ;
        Util.raise_semantic_error acc.loc
          (Printf.sprintf "Variable %s not found" id) 
    )
  )
  | AccDeref ex -> (
      logger#debug "Access reference checking %s ..."
        (show_expr_node ex.node) ;
      let tipe = check_exp ex in
      match tipe with
      | Ast.TypPoint point_tipe -> point_tipe
      | _ ->
          Util.raise_semantic_error acc.loc
            (Printf.sprintf
               "Invalid operator, to use '*' the type need to be a pointer" )
      )
  | AccIndex (acc, exp_1) -> (
      logger#debug "Access index checking ..." ;
      let _ = check_acc acc in
      let type_index = check_exp exp_1 in
      match type_index with
      | Ast.TypInt as t -> t
      | _ ->
          logger#debug "Type index type on array has type different to int" ;
          Util.raise_semantic_error acc.loc
            (Printf.sprintf "Type index as type %s but is expected an int"
               (Util.convert_type_to_str type_index) ) )

and check_exp exp =
  match exp.node with
  | Access acc ->
      logger#debug "Checking access" ;
      check_acc acc
  | Addr acc ->
      logger#debug "Checking Addr" ;
      let tipe = check_acc acc in
      Ast.TypPoint tipe
  | Assign (acc, exp) ->
      logger#debug "Checking assign" ;
      let type_access = check_acc acc in
      logger#debug "Type access %s" (Util.convert_type_to_str type_access) ;
      let type_to_assign = check_exp exp in
      check_var_type_allowed type_to_assign exp ;
      if type_access <> type_to_assign then check_mem_acc type_access exp
      else type_to_assign
  | Ast.DecAssig (dec, expr) ->
      logger#debug "* int i = 0;* Translating" ;
      let _ = check_blk dec in
      check_exp expr
  | ILiteral vl ->
      logger#debug "Integer found" ;
      Ast.TypInt
  | CLiteral vl -> logger#debug "Char found" ; Ast.TypChar
  | NullLiteral t ->
      logger#debug "Null type found" ;
      t
  | BLiteral vl ->
      logger#debug "Boolean found" ;
      Ast.TypBool
  | UnaryOp (unary_op, exp) -> (
      logger#debug "Unary operation found" ;
      let type_ex = check_exp exp in
      match unary_op with
      | Neg | PreInc | PostInc | PreDec | PostDec -> (
        match type_ex with
        | Ast.TypInt -> type_ex
        | Ast.TypPoint tipe ->
            if tipe = Ast.TypInt then tipe
            else (
              logger#debug "Type %s but expected numerical type"
                (Util.convert_type_to_str type_ex) ;
              Util.raise_semantic_error exp.loc
                (Printf.sprintf "Type %s but expected numerical type"
                   (Util.convert_type_to_str type_ex) ) )
        | _ ->
            logger#debug "Type %s but expected numerical type"
              (Util.convert_type_to_str type_ex) ;
            Util.raise_semantic_error exp.loc
              (Printf.sprintf "Type %s but expected numerical type"
                 (Util.convert_type_to_str type_ex) ) )
      | Not -> (
        match type_ex with
        | Ast.TypBool -> type_ex
        | _ ->
            logger#debug "Type %s but expected bool type"
              (Util.convert_type_to_str type_ex) ;
            Util.raise_semantic_error exp.loc
              (Printf.sprintf "Type %s but expected bool type"
                 (Util.convert_type_to_str type_ex) ) ) )
  | BinaryOp (bin_op, exp1, exp2) -> (
      logger#debug "Checking binary operation" ;
      let exp1_type = check_exp exp1 in
      let exp2_type = check_exp exp2 in
      match bin_op with
      | Ast.Geq | Ast.Leq | Ast.Greater 
      | Ast.Comma | Ast.Less | Ast.Equal
      |Ast.Neq -> (
          if check_same_basic_type exp1_type exp2_type then Ast.TypBool
          else
            let are_same = Util.compare_type exp1_type exp2_type in
            if are_same then
              Ast.TypBool
            else (
              logger#debug "Type mismatch, you can not make an operation between \
              different type %s is different from %s" (Util.convert_type_to_str exp1_type)
                        (Util.convert_type_to_str exp2_type) ;
                Util.raise_semantic_error exp.loc
                (Printf.sprintf
                   "Type mismatch, you can not make an operation between \
                    different type %s is different from %s"
                   (Util.convert_type_to_str exp1_type)
                   (Util.convert_type_to_str exp2_type) )
            )
      )
      | Ast.And | Ast.Or ->
          if exp1_type = Ast.TypBool && exp2_type = Ast.TypBool then
            Ast.TypBool
          else (
            logger#debug "Type mismatch, you can not make an operation between \
            different type %s is different from %s" (Util.convert_type_to_str exp1_type)
                      (Util.convert_type_to_str exp2_type) ;
              Util.raise_semantic_error exp.loc
              (Printf.sprintf
                 "Type mismatch, you can not make an operation between \
                  different type %s is different from %s"
                 (Util.convert_type_to_str exp1_type)
                 (Util.convert_type_to_str exp2_type) )
          )
      | Ast.Add | Ast.Sub | Ast.Mult | Ast.Div | Ast.Mod ->
          let are_pointers =
            Util.operation_between_pointers exp1_type exp2_type
          in
          if are_pointers = false && exp1_type = exp2_type then exp1_type
          else if Util.operation_between_pointers exp1_type exp2_type then (
            logger#debug "Operation between pointer not allowed" ;
            Util.raise_semantic_error exp.loc
              (Printf.sprintf "Operation between pointer not allowed") )
          else (
            logger#debug
              "Type mismatch inside the binary operation, you can assign \
               only variable with the same type" ;
            Util.raise_semantic_error exp.loc
              (Printf.sprintf
                 "Type mismatch, you can not make an operation between \
                  different type %s is different from %s"
                 (Util.convert_type_to_str exp1_type)
                 (Util.convert_type_to_str exp2_type) ) ) )
  | Call (id, exps) -> (
      logger#debug "Call ***%s*** checking ..." id ;
      try (
      let function_type = Symbol_table.lookup id !global_scope in
      let _ =
        List.mapi
          (fun index exp ->
            let exp_res = check_exp exp in
            let tipe = id >? index in
            if Util.compare_type tipe exp_res then exp_res
            else (
              logger#debug
                "Parameter type expected is %s but an type %s is found"
                (Util.convert_type_to_str tipe)
                (Util.convert_type_to_str exp_res) ;
              Util.raise_semantic_error exp.loc
                (Printf.sprintf
                   "Parameter type expected is %s but an type %s is found \
                    at position %d"
                   (Util.convert_type_to_str exp_res)
                   (Util.convert_type_to_str tipe)
                   index ) ) )
          exps
      in
      function_type
      ) with NotFoundInScope -> (
        logger#debug "Function %s not found" id ;
        Util.raise_semantic_error exp.loc
          (Printf.sprintf "Function %s not found" id) 
      )
    )

and check_blk blkstm =
  match blkstm.node with
  | Ast.Dec (tipe, id) ->
      logger#debug "Variable declaration checking ..." ;
      check_var_type_allowed tipe blkstm ;
      check_arry_type tipe blkstm ;
      Symbol_table.add_entry id tipe !global_scope
  | Ast.Stmt stm ->
      logger#debug "Stm checking ..." ;
      check_stm stm

and check_stm node =
  match node.node with
  | Ast.If (ex, ifs, els) ->
      logger#debug "If stm checking .." ;
      let tipe_ex = check_exp ex in
      if tipe_ex <> Ast.TypBool then
        Util.raise_semantic_error node.loc
          "If condition need to have a bool type"
      else
        global_scope :=
          Symbol_table.begin_block !global_scope
            (Symbol_table.get_type !global_scope) ;
      check_stm ifs ;
      global_scope := Symbol_table.end_block !global_scope ;
      global_scope :=
        Symbol_table.begin_block !global_scope
          (Symbol_table.get_type !global_scope) ;
      check_stm els ;
      global_scope := Symbol_table.end_block !global_scope ;
      ()
  | Ast.While (ex, stm) ->
      logger#debug "While stm checking ..." ;
      let tipe = check_exp ex in
      if tipe <> Ast.TypBool then
        Util.raise_semantic_error node.loc
          "While condition need to be a bool type"
      else
        global_scope :=
          Symbol_table.begin_block !global_scope
            (Symbol_table.get_type !global_scope) ;
      check_stm stm ;
      global_scope := Symbol_table.end_block !global_scope ;
      ()
  | Ast.DoWhile (ex, stm) ->
      logger#debug "Do while stm checking ..." ;
      global_scope :=
        Symbol_table.begin_block !global_scope
          (Symbol_table.get_type !global_scope) ;
      check_stm stm ;
      let tipe_ex = check_exp ex in
      if tipe_ex <> Ast.TypBool then (
        global_scope := Symbol_table.end_block !global_scope ;
        Util.raise_semantic_error node.loc
          "Do While condition need to be a bool type" )
      else global_scope := Symbol_table.end_block !global_scope ;
      ()
  | Ast.Expr ex ->
      logger#debug "Expression checking ..." ;
      let _ = check_exp ex in
      ()
  | Ast.Return optex -> (
      logger#debug "Return stm check" ;
      let return_type = Symbol_table.get_type !global_scope in
      match optex with
      | Some v ->
          logger#debug "Check Return stm with value (same of the function)" ;
          let exp_type = check_exp v in
          check_return_type exp_type v ;
          if exp_type <> return_type then
            Util.raise_semantic_error node.loc
              (Printf.sprintf "Type mismetch aspect %s but it is %s"
                 (Util.convert_type_to_str return_type)
                 (Util.convert_type_to_str exp_type) )
          else ()
      | None ->
          logger#debug
            "Check Return stm if the the function is a void function" ;
          if return_type <> Ast.TypVoid then
            Util.raise_semantic_error node.loc "Type aspected is void"
          else () )
  | Ast.Block blkstm -> List.iter check_blk blkstm

and unwrap_op_exp node =
  match node with Some v -> check_exp v | None -> Ast.TypVoid

let add_var fun_name pos var ast_node =
  logger#debug "Adding element with position %d" pos ;
  match var with
  | tipe, id -> (
    try
      check_var_type_allowed tipe ast_node ;
      Symbol_table.add_entry id tipe !global_scope ;
      ( >-> ) fun_name pos tipe ast_node.loc
    with DuplicateEntry ->
      logger#debug "Variable %s declaration duplicate" id ;
      Util.raise_semantic_error ast_node.loc
        ("Variable " ^ id ^ " duplicated in declaration statment") )

let check_fundec node ast_elem =
  match node with
  | fun_decl as f ->
      logger#debug "Checking function declaration %s" f.fname ;
      check_function_type_allowed f.typ ;
      Symbol_table.add_entry f.fname f.typ !global_scope ;
      global_scope := Symbol_table.begin_block !global_scope f.typ ;
      let rec iterate elem_list ast_node count =
        match elem_list with
        | [] -> ()
        | elem :: list_elem ->
            add_var f.fname count elem ast_node ;
            iterate list_elem ast_node (count + 1)
      in
      iterate f.formals ast_elem 0 ;
      check_stm f.body ;
      global_scope := Symbol_table.end_block !global_scope ;
      ()

let rec match_type ast_elem =
  match ast_elem.node with
  | Vardec (tipe, id) ->
      logger#debug "Global variable found" ;
      check_var_type_allowed tipe ast_elem ;
      check_arry_type tipe ast_elem ;
      Symbol_table.add_entry id tipe !global_scope
  | DecAssign (dec, exp) ->
      logger#debug "Global variable found with assignment" ;
      match_type dec ;
      let _ = check_exp exp in
      ()
  | Fundecl fundec ->
      logger#debug "Function analysis found" ;
      check_fundec fundec ast_elem

let check (Ast.Prog topdecls) =
  List.iter match_type topdecls ;
  try Symbol_table.lookup "main" !global_scope
  with _ -> failwith "No function main inside the program"
