// author Vincenzo Palazzo v.palazzo1@stundeti.unipi.it

int main()
{
	int x[1];
	int a;

	x[0] = 10;
	a = --x[0];

	print(a);
	print(x[0]);
	return 0;
}
