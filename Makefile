TARGET=microcc

default: $(TARGET).native

check: default
	pytest

$TARGET: default

native: $(TARGET).native

%.native:
	ocamlbuild -use-ocamlfind $@
	mv $@ $*

clean:
	ocamlbuild -clean
	rm -rf *.o *.bc *.s *.out

ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

install:
	cp $(TARGET) $(PREFIX)/bin

uninstall:
	rm $(PREFIX)/bin/$(TARGET)

.PHONY: clean default check
