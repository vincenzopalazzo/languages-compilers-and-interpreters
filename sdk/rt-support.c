#include <stdio.h>
#include <stdlib.h>

int getint(){
  int tmp_val;
  scanf("%d", &tmp_val);
  return tmp_val;
}

void print(int n){
  printf("%d\n", n);
}
