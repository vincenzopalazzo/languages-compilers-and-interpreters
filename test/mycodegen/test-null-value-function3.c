// Author Vincenzo Palazzo v.palazzo@stundeti.unipi.it
#include <stdio.h>

int max(char *x, char *y)
{
	if (x == NULL || y == NULL) {
		return -1;
	}
	if (*x == *y)
		return 1;
	return 0;
}

int main()
{
	char *x = NULL;
	char *y = NULL;
	char a = 'a';
	y = &a;
	printf("%d\n", max(x, y));
	x = &a;
	printf("%d\n", max(x, y));
	return 0;
}