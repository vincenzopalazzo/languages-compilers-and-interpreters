// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

// To make sure that the error
// is correct throws from the compiler
// uncommnet this line
// int b;

int main()
{
	do {
		int a;
		a = 1;
		// Error from semantics check, the b don't ext
	} while (b < 1);
}
