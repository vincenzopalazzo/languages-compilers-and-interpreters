// author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int x;
	int a;

	x = 10;
	a = --x;

	print(a);
	print(x);
	return 0;
}
