// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
#include <stdio.h>

int increment(int a)
{
	a++;
	return a;
}

void increment_by_ref(int *a) { (*a)++; }

int main()
{
	int a;
	a = 0;
	a = increment(a);
	printf("%d\n", a);
	increment_by_ref(&a);
	printf("%d\n", a);
	return 0;
}
