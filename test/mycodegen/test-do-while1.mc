// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int main()
{
	int a;
	a = 0;
	do {
		a++;
		print(a);
	} while (a < 20);
	return 0;
}
