// author Vincenzo Palazzo v.palazzo1@stundeti.unipi.it
#include <stdio.h>
int main()
{
	int x[1];
	int a;

	x[0] = 10;
	a = ++x[0];

	printf("%d\n", a);
	printf("%d\n", x[0]);
	return 0;
}
