#include <stdio.h>

int *P;

int max(int x, int y)
{
	if (*P > y)
		return x;
	return y;
}

int A;

int main()
{
	A = 12;
	P = &A;
	A = max(12, 1001);
	printf("%d\n", *P);
	printf("%d\n", A);
	return 0;
}
