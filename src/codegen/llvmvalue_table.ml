(* *
   * MicroC Compiler developer with University of Pisa for
   * courses Languages, Compilers and Interpreters class.
   *
   * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
   * and all professors of the Languages, Compilers and Interpreters class
   * https://github.com/lillo/compiler-course-unipi
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *)
  
open Ast
open Easy_logging

exception DuplicateEntry

exception NotFoundInScope

let logger = Logging.make_logger "LLVMValue_table" Info [Cli Debug]

type dec =
  { name: string
  ; variables: (Ast.identifier, Llvm.llvalue) Hashtbl.t
  ; parent: dec option
  ; mutable ended: bool }

let empty_table =
  {name= "global"; variables= Hashtbl.create 0; parent= None; ended= false}

let begin_block table name_scope =
  logger#trace "Starting scope" ;
  { name= name_scope
  ; variables= Hashtbl.create 0
  ; parent= Some table
  ; ended= false }

let end_block table =
  logger#trace "Ending scope with %d variables"
    (Hashtbl.length table.variables) ;
  match table.parent with
  | Some v ->
      logger#trace "Block with %d variable deleted"
        (Hashtbl.length v.variables) ;
      v
  | None -> table

let rec lookup symbol table =
  let wanted_var = Hashtbl.find_opt table.variables symbol in
  match wanted_var with
  | Some v -> v
  | None -> (
    match table.parent with
    | Some v -> lookup symbol v
    | None -> raise NotFoundInScope )

let add_entry symbol info table fun_parm =
  let variable = Hashtbl.find_opt table.variables symbol in
  match variable with
  | Some v -> raise DuplicateEntry
  | None -> Hashtbl.add table.variables symbol info

let rec get_name_scope table : string =
  match table.name with
  | "if" | "else" | "while" | "for" | "do-while" -> (
    match table.parent with
    | Some v -> get_name_scope v
    | None -> raise NotFoundInScope )
  | _ -> table.name

let set_end_block table value =
  logger#debug "Change value" ;
  table.ended <- true ;
  logger#debug "Change value %s" (if table.ended then "true" else "false")

let is_ended table = table.ended

let is_function table =
  match table.name with
  | "if" | "else" | "while" | "for" | "do-while" -> false
  | _ -> true
