// Author Vincenzo Palazzo v.palazzo1@studenti.unipi.it

int increment_value(int a)
{
	a++;
	return a;
}

int main()
{
	int a;
	a = 0;
	do {
		a = increment_value(a);
		print(a);
	} while (a < 20);
	return 0;
}
